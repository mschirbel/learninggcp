# Google Cloud Architect 1

1. Account
    - DC Infra
    - Free Trial
    - Overview
    - Organization & Projects
    - Interaction with GCP
    - Cloud Shell
    - REST API
2. Compute
    - Compute Options
    - Compute Engine
    - Connect Windows
    - Manage Disks
    - Snapshots
    - Cloud Launcher
    - App Engine
3. Network
    - Overview
    - VPC
    - IP Address & Firewall
4. Data
    - Big Data
    - Big Query
    - Machine Learning
5. Containers
    - Overview
    - GKE
6. Storage
    - Storage Options
    - Database Options
    - Cloud Storage
    - Cloud SQL
    - Bigtable
    - Cloud Spanner
    - Integrations of Cloud Storage
