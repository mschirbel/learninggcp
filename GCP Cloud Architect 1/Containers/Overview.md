# Overview

Container são uma forma de isolar um ambiente, incluindo códigos de aplicações, em blocos altamente independentes do SO por baixo das dependências.
Assim, podemos desacoplar a aplicação do sistema operacional e levar para outras máquinas, desde que a mesma engine de containers esteja funcionando.

![](../media/container1.png)

Temos mais tempo para fazer o *load*, pois não temos um SO. Os tamanhos são menores e podem ser quebrado em módulos menores para mais fácil administração.

Para uma engine, usamos o Docker. E para administrar uma grande quantidade de containers, usamos o Kubernetes.

Kubernetes vem do grego, do que hoje chamamos de *helmsman*, que é o piloto de um barco cargueiro.
É open-source, e usado pelo Google internamente.

Para isso, podemos fazer o deploy de aplicações de uma forma rápida e sem downtime, pagando somente aquilo que usamos.

### Concepts for K8s

Master - controla os nodes
Nodes -  que faz as tarefas do K8s
Pods - grupo de um ou mais containers, que dividem o mesmo IP, Hostname e recursos.
Replication Controller - assegura que o número de replicas de pods está rodando no K8s inteiro
Kubectl - linha de comando para o K8s.