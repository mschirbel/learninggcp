# Google Container Engine

É um ambiente totalmente gerenciado pelo Google para aplicações em containers.
Temos algumas características:
- Self Healing
- Autoscaling
- Usa Kubernetes
- SO customizado para rodar o K8s
- Usa recursos do Compute Engine

Em termos de administração, o GKE cai bem no meio:

![](../media/container2.png)

Usamos esse container quando temos:
1. um ambiente multicloud
2. usamos protocolos alem de `https`
3. precisamos de orquestração de containers


Usamos o compute engine quando:
1. precisamos de GPU
2. nao temos aplicações em containers
3. estamos migrando do on-prem
4. precisamos de algum SO específico.

Caso precisamos de algum tutorial, temos uma pasta aqui chamada `kubernetes the hard way`, podemos ver nela o necessário.

## Concepts

1. Container Cluster
    - grupo de compute engines rodando K8s
    - contem 1 ou mais nodes e um master endpoint para comunicação
2. Pods
    - grupo de um ou mais containers
    - dividem configurações e recursos
    - são efêmeros
    - podem aumentar ou diminuir
3. Nodes
    - são as instâncias de compute engine
    - podem ter diversos pods
4. Replication Controler
    - automaticamente aumenta ou diminui os pods conforme necessidade
5. Services
    - Define um acesso a aplicações de pods usando somente um IP/DNS e uma porta
    - cria uma abstração para acesso a pods
6. Container Registry
    - Seguro e prático
    - armazenar imagens de Docker

![](../media/container3.png)

---

## Create a Cluster

Para criar um cluster de container, usamos o menu principal:

![](../media/container4.png)

Para as informações, precisamos selecionar as regions, tipo de imagem, versão do Kubernetes e informações de logging, monitoração:

![](../media/container5.png)

Nosso cluster pode estar em mais zones, para ter redundância e FT:

![](../media/container6.png)
Isso vai multiplicar a quantidade de nodes que temos.
Podemos também habilitar a integração com outros serviços:

![](../media/container7.png)

Geralmente, devemos fazer a substituição automática de imagens em nossos manifestos, isso pode ser feito do seguinte jeito:

```
sed -i s/imageid/$NEWIMAGE/ manifest.yaml
```
Isso usa uma variável de ambiente chamada `$NEWIMAGE`, com o valor da nova image.

Para fazer o deploy em um cluster:

```
docker build -t <NOVA IMAGEM>
gcloud docker -- push gcr.io/<NOME DO REGISTRY>/<NOME DO REPOSITORIO>
gcloud container cluster get-credentials <NOME DO CLUSTER> --zone <ZONA DO CLUSTER> --project <PROJECTO>
kubectl create -f <ARQUIVO DO MANIFESTO.yaml>
kubectl get pods
kubectl get svc
```

Quando fizemos o deploy do GKE, temos novas máquinas no Compute Engine:

![](../media/container8.png)