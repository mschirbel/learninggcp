# Free Trial

Existem dois tipos de free trial para o GCP.
Existem o **12Months** e o **Always Free**.

O bom é que um faz overlap com o outro, assim podemos acabar não gastando nossos $300 de crédito. E mesmo que os 12 meses acabem, ainda temos o always free plan.

## 12 Months

Temos 1 ano e $300 de crédito para usarmos nos produtos. O trial termina se:
    - os creditos acabam
    - os 12 meses passam

Mas mesmo assim, precisamos de um cartão de crédito. E temos alguns limites:
    1. não mais do que 8 cores em todas as suas instâncias
    2. não mais do que 100GB de SSD
    3. não mais do que 2TB do que disk space.

## Always Free

Ele só funciona para as regions do US.
Esse tier, é só para recursos em pequena escala. E temos alguns limites, por exemplo, no Compute:
    1. só podemos usar uma *f1-micro* instância por mês - só para US
    2. 30GB HDD
    3. 5GB Snapshots

## Account

Precisamos de uma conta Google para usar o GCP.