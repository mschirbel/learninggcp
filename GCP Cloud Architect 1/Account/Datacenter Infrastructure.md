# DC Infra

Temos 4 componentes:
1. Datacentes
    funcionam em regions e zones - assim temos a capacidade de escalabilidade
2. Backbone
    o que conecta os datacenters por meio de uma rede de fibra ótica
3. Points of Presence
    pontos no mundo de conexões a internet, para dentro da rede do Google
4. Edge Nodes
    pontos mais próximos aos usuários, geralmente usados para cache

Podemos ver aqui os datacenters:

![](../media/datacenters.png)

Provavelmente isso já mudou, pois a expansão do Google é muito rápida. Mas o funcionamento do GCP é assim:

![](../media/infra-elements.png)
No meio temos os core data centers, que são as zones. Depois temos os PoP(Points of Presence) e por fim temos os Edge Nodes, que são destinados ao cache.

Points of Presence são mais de 90 localidades e traz o tráfego mais próximo aos usuários.

Edge Nodes é o ponto mais próximo do usuário, também chamado de Edge Cache. isso é usado via ISP, usando componentes do Google dentro da infra deles.

### Regions

Regions são locais geograficamente espalhados onde podemos ter nossos recursos. São um conjunto de Zones. Existem recursos que são disponíveis por region, ou seja, para as zones daquela region.

### Zones

São datacenters isolados dentro de uma region. A intenção de ter múltiplas zones dentro de uma region é combater o *single-point-of-failure*, pois os recursos podem estar em múltiplas zones.

## Environment

Toda energia usada para o GCP é energia renovável, desde 2007 eles são *carbon neutral*.

## Backbone

Podemos ver no mapa abaixo como funciona a rede de fibra ótica.

![](../media/backbone.png)

