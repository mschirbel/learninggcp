# Cloud Shell

Cloud Shell é uma instância dentro da sua console do GCP, nesse shell temos uma instância, da nossa conta, que tem o SDK já instalado e configurado para o uso.

É feito para uso do SDK, qualquer uso pesado de CPU/Network a Google vai cancelar sua instância. E deve ser mantida uma sessão na instância.

## Comandos do SDK

Podemos criar, por exemplo, uma nova instância:

```
gcloud compute instance create instance-1 --zone us-central1-a
```

Podemos ver todos os comando ![aqui](https://cloud.google.com/sdk/gcloud/reference).

![](../media/gcpshell.png)