# Interaction

Temos algumas formas de acessar a GCP:

- web console
- sdk
- gcp rest api

### Console

É onde é centralizado todos os projetos em GCP, e ainda temos acesso ao Google Cloud Shell.
Temos um dashboard dos recursos, billing e acesso a logs de acesso. Temos as docs, tutoriais e novidades do GCP.

### SDK

Temos a CLI para acesso a GCP. Temos algumas como:
- gcloud - para a maioria dos recursos do GCP
- gsutil - acesso ao cloud storage
- bq - acesso ao Big Query

### Cloud Shell

O acesso é feito de dentro da console, e não precisamos instalar nenhum SDK e sempre está disponível quando precisamos, mesmo em outro computador.
Para isso, temos acesso a uma máquina virtual, acessada do browser. Temos 5GB de storage, com a SDK já instalada. Ou seja, os arquivos se mantém, caso troquemos de PC.

Temos suporte a:
- python
- nodejs
- java
- go
- php
- ruby

E temos authorization nativa ao console e projetos.

![](../media/gshell.png)

### Restful API

Temos acesso programático a GCP usando uma API, que conversa em JSON e usa OAUTH 2.0 para authentication e authorization.
Deve ser habilitada via console e tem uma capacidade nativa, mas podemos solicitar mais.