# Restful API

Podemos ver algumas aplicações, clicando ![aqui](https://developers.google.com/apis-explorer)

Nesse link temos alguns exemplos de API, não somente para o GCP.

Podemos inclusive testar a API:

![](../media/gcloud-api-test.png)

Com isso teremos o GET usado, o que podemos usar no `curl` e depois a *reponse* da API do Google.