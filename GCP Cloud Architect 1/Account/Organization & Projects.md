# Organization & Projects

A hierarquia de projetos e organizações dentro do GCP é conhecida como **Cloud Resouce Hierarchy**. Assim como num OS, cada pasta filha embaixo da pasta pai só tem e somente um pai, e o seu acesso deriva da pasta pai.

No topo da hierarquia temos uma única Organization. Embaixo temos os projetos, e em cada projeto temos os recursos envolvidos.

![](../media/org-proj.png)

Não podemos criar nenhum recurso sem estar associado com um projeto. Seja acessos, billing, api, e qualquer outra coisa, precisamos de um project.

### Project Attributes

Precisamos de:
- Project Name - nome amigável e pode ser trocado
- Project ID - é **único** em toda a GCP e não pode ser trocado depois de criado. Mas podemos editar na criação do projeto.
- Project Number - é mais usado para services para interações

Podemos criar projetos ao clicar em New Project:

![](../media/newproj.png)

Para acessar as informações de Projetos, podemos acessar no menu do topo, assim temos todos os projetos que temos nessa organização:

![](../media/view-proj.png)

Não temos uma organização, pois entramos com uma conta de Trial, logo, nosso e-mail cumpre o papel. Mas se tivermos um domínio, assim poderemos ter uma organização referente.

Podemos acessar também as permissões do projeto usando o IAM:

![](../media/iam1.png)

Logo na Dashboard(que é a página inicial do GCP) temos as informações básicas do Projeto, no qual podemos clicar em *Go to Project Settings*.

Nesses menus podemos acessar as informações dos projetos e inclusive deletá-los.

![](../media/delete-proj.png)

Quando deletamos(shut down) todos os recursos dentro do projeto serão deletados. E receberemos um aviso que o projeto será deletado em 30d, por padrão. Nesse aviso, enviado por e-mail, temos um link no qual podemos fazer o restore do projeto.