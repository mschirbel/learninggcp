# IAM

Temos a implementação do GCP do IAM(Identity and Access Management)
    - memberrs
    - roles
    - resources
    - policies
    - hierarchy

Para toda organização precisamos saber:
- quem tem acesso(who)
- qual acesso cada pessoa tem(what)
- quais recursos ela pode controlar(which)

Isso vai além do GCP, mas é uma medida de segurança geral. Para isso temos o princípio do *least privilege*, que diz que damos o mínimo controle necessário.

Para o GCP, temos o Cloud IAM:

![](../media/iam2.png)

Por meio desse serviço, podemos controlar o acesso de nossos projetos.

### Member

Para isso, temos **membros**, que podem ser uma pessoa ou um *service account*. Para pessoas podem ser:
    - google account
    - google group
    - g suite domain
    - Cloud Identity Domain - organization que não é um domínio do google.

Para os service accounts, temos o exemplo de acesso de uma aplicação. Por exemplo um serviço de backup que quer usar o Cloud Storage.

Services accounts, usam e-mail para acessar, geralmente fica nesse formato:
`<project_number>@developer.gserviceaccount.com` ou `<project_id>@developer.gserviceaccount.com`.

### Role

Uma coleção de permissões dadas para acesso a um recurso. Assumimos uma role, temporariamente, para o acesso.
Geralmente as permissões ficam no formato:

`<service>.<resource>.<verb>`. Por exemplo `compute.instance.delete` ou `compute.instance.start`.

Existem dois tipos de Roles:
1. primitive: são nativas ao GCP, mesmo antes de existir o IAM. Só podem ser usadas para permissões em projetos, não funcionam para acessos granulares. Podemos ter 3 roles;
    - viewer - só pode ver um project e seus recursos
    - editor - pode modificar acessos em recursos
    - owner - pode deletar e modificar o billing

2. predifined: podem dar acesso granular a recursos. Por exemplo o `App Engine Admin`, que dá acesso aos recursos a um determinado service.

Podemos ler mais ![aqui](https://cloud.google.com/iam/docs/understanding-custom-roles)

### Hierarchy

Ficamos com o seguinte

*Organization* -> *Project* -> *Resource*.
Cada filho, tem somente um pai, pois é uma árvore. Os filhos herdam as policies dos pais.

---

Podemos criar os acessos indo em IAM, veja que temos os acessos definidos aqui:

![](../media/iam3.png)

E aqui podemos ver as service accounts:

![](../media/iam4.png)

Aqui podemos editar as roles primitivas e as predefinidas também. Se selecionarmos Project temos as *primitives*, todas as outras são *predefined*.

![](../media/iam5.png)

Para uma role, podemos fazer o *disable*, e não somente deletar.