# GCP

As vantagens da cloud computing:
- pagamos o que usamos
- mais inovação
- mais produtividade e eficiência
- elasticidade vertical e horizontal

Para o GCP(Google Cloud Plataform) é o serviço de cloud computing do Google que funciona na mesma infraestrutura do Google. Com diversos datacenters e com tudo gerenciado pelo google.

Existem algumas categorias:

![](../media/overview1.png)

Uma das vantagens do GCP, temos um pricing melhor, pois somos cobrados *por minuto*. Além de termos descontos automáticos, sem necessidade de contratos.

Google também tem uma rede de fibra ótica(backbone), entre os seus datacenters. Por isso, temos a possibilidade de fazer migrações de VMs sem downtime. Além de alguns estudos que mostram benchmarks melhores do que outros providers.