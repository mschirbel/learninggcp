# Integrations of Cloud Storage

## App Engine

Uma ideia de como podemos fazer nossa aplicação seria:

![](../media/integration1.png)

Para criar o bucket, podemos usar a command line. Abra o Cloud Shell dentro do project que vc deseja criar o bucket e digite:

```
gsutil mb -l US gs://<NAME>
```

Quando definimos a *location*, com o parâmetro `-l`, estamos definindo também a storage class:

```
gsutil mb -l US          # multiregional
gsutil mb -l us-central1 # regional
```

Caso precisemos de outra classe, usamos o parâmetro `-c`.

Agora, damos permissão de read:

```
gsutil defacl ch -u AllUsers:R gs://<NAME>
```

`defacl` é para deficinir o ACL do bucket. `ch` é para change.

Depois, podemos trocar no código para acessar os dados no bucket, visto que ele está visível para o mundo.

---

## Third Party

Geralmente, alguma ferramenta de terceiro que precisa de acesso a um recurso GCP, necessitará de login com OAuth2.0. Para isso, será necessário realizar o login na sua conta Google e receber uma *key* para o acesso:

![](../media/integration2.png)