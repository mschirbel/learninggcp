# Cloud SQL

Ao criarmos uma instância, podemos escolher entre 3 opções:

![](../media/sql1.png)

Depois temos que preencher algumas informações sobre o SGBD

![](../media/sql2.png)

E ainda temos algumas opções avançadas, como rede, storage e tipo de máquina:

![](../media/sql3.png)
Podemos inclusive usar um storage que aumenta o limite de forma automatizada.

Ao subir uma máquina e clicar no nome dela, podemos ver algumas métricas:

![](../media/sql4.png)

Para conectar em uma instância podemos usar alguma IDE ou o `gcloud`:

```
gcloud sql connect <NOME DA INSTANCE> --user=root
```

Entramos com a senha e estaremos prontos para usar o BD.