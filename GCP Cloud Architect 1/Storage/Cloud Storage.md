# Cloud Storage

Cloud Storage é um serviço para armazenar dados que não estão estruturados. Isso seriam:
    - fotos
    - videos
    - documentos
    - textos
    - backups

É referenciado como um `BLOB` storage.

Cloud Storage se interage bem com outros serviços:

![](../media/storage3.png)

Com isso, CStorage tem um *unified object storage*, que indepente do formato do objeto para trazer a disponibilidade. É bem barato, assim como o S3 da AWS, e totalmente escalável.

Não é um File System, mas pode ser usado com outras ferramentas.
Podemos ter criptografia, seja *in transit* ou *at rest*.

### Conceitos

Temos alguns conceitos envolvidos no Cloud Storage:
1. Bucket - que é o container que fica os dados. E deve ser algo único em toda a GCP
2. Objects - são os arquivos mantidos dentro do bucket.
3. Data Opacity - é a falta de estrutura de um bucket.

#### Classes

1. Multi-Regional. Redundante no mundo. Ótimo para videos, games e conteúdos
2. Regional. Limitado por região. Bom para analytics
3. Nearline. Pouco custo por GB e precisam durar 30d no bucket e tem custo de recuperação
4. Coldline - Menor custo por GB e precisam durar 90d no bucket.

Todas sem o mesmo throughtput, low latency e high durability. A diferença está na disponibilidade, duração e preço.

![](../media/storage4.png)

Se tentarmos trocar o tipo de class storage, não podemos voltar na escala(caso selecionamos a region):
`Multi-Regional -> Regional -> Nearline -> Coldline`

E se trocarmos a class do bucket, somente os objetos novos estarão na nova classe, os antigos permanecem na antiga.

---

Ao criar um Bucket, temos que tomar cuidado com o nome único e com a class:

![](../media/storage5.png)

Ao escolhermos uma região, não escolhemos a zone, porque os dados são replicados com redundância dentro da region. Depois podemos fazer operações básicas, como Upload, Download, Folder Creation, Move Object e etc.