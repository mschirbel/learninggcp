# Storage Options

GCP tem alguns serviços para fornecer storage de formas versáteis ao usuário. Existem alguns tipos:

1. Bigtable
2. Datastore
3. Storage
4. SQL
5. Spanner

Para saber qual usar, podemos seguir o fluxo:

![](../media/storage1.png)