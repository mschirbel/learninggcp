# Cloud Spanner

Cloud Spanner é um banco de dados relacional, com uma alta capacidade de escalar horizontalmente.

![](../media/spanner1.png)

Ao criar uma instância, podemos principalmente selecionar o número de nodes:

![](../media/spanner2.png)

Podemos ver informações em tempo real, métricas, e uso de hardware:

![](../media/spanner3.png)
Dentro do Spanner, podemos criar um database, com ou sem um *schema*.

![](../media/spanner4.png)
Com o DB dentro do Spanner, ele é mapeado o suficiente para ser escalado horizontalmente. Ainda assim, podemos criar as tabelas via console web:

![](../media/spanner5.png)

Ou podemos simplesmente criar uma formulário da console web.

![](../media/spanner6.png)

E agora podemos fazer queries:

![](../media/spanner7.png)
