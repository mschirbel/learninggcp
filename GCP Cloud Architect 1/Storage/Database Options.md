# Database Options

Como forma de banco de dados existem quatro categorias:

1. Cloud SQL - banco relacional -> web, ecommerce, management system
2. BigTable -  banco não relacional -> analytics, iot, ad tech
3. Datastore - banco não relacional com aspectos relacionais -> user profile, game states
4. Spanner -> financial services, supply chain

## Cloud SQL

Criamos uma instância usando alguma engina já conhecida como MySQL. 
A gestão da máquina que sustenta a engine, é feita pela GCP, então não precisamos fazer updates ou patches.

Podemos fazer 2 tipos de scaling:
    - vertical(read/write)
    - hortizontal(read)

Mas isso impõe certa escalabilidade limitada.

## Datastore

É bem flexível e escalável até terabytes de dados. Não escolhemos nem a máquina e nem o disco. Tudo é gerenciado.

## Bigtable

É um banco NoSQL para armazenar até petabytes de dados. É usado para o Gmail e Google Analytics.
Podemos escrever grandes volumes em milisegundos, entretanto é mais caro que o Datastore.

## Spanner

É um banco relacional mas ele escala muito bem horizontalmente.

![](../media/storage2.png)