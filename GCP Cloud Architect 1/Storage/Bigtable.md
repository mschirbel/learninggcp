# Bigtable

Para acessar o Bigtable podemos fazer via menu:

![](../media/bigtable1.png)

Depois, podemos selecionar algumas opções, inclusive se vamos usar cluster, que serve para o processamento de grandes quantidades de dados.

![](../media/bigtable2.png)

Mas quem faz o processamento da informação é o node. E podemos marcar qual a quantidade de performance desejamos ter em nosso ambiente:

![](../media/bigtable3.png)

Essas máquinas são gerenciadas pela GCP e não temos acesso. Por fim, temos o custo de Storage e o custo das máquinas do ambiente. Percebemos que esse serviço é para grandes empresas que tem grandes quantidades de dados.

Para conectar no cluster de Bigtable, podemos usar o Cloud Shell. Nesse shell temos o `cbt` que é um `gcloud` modificado. Ou mesmo baixar um script:

```
curl -fO https://storage.googleapis.com/cloud-bigtable/quickstart/GoogCloudBigtable-Quickstart-1.0.0-pre2.zip
unzip GoogCloudBigtable-Quickstart-1.0.0-pre2.zip
cd quistart
./quickstart.sh
```

Agora inserimos as informações sobre o serviço.