# Connect to a Windows Instance

Para conectar em uma instância Windows, precisamos ativar regras de Firewall, para liberar a porta de Remote Desktop.

Perceba que quando habilitamos HTTP e HTTPS não temos o comando de regra de firewall no `gcloud`:

![](../media/windows1.png)

Entretanto, agora temos duas tags. Mas por que? Porque as regras não tinham sido criadas ainda, mas como as regras já existem, não precisamos recriar, apenas **precisamos adicionar as tags**:

`--tags "http-server", "https-server"`

Isso acontece porque nas regras de firewall temos um *target*, que são definidos pelas tags:

![](../media/windows2.png)

Repare que agora as regras existem e para definirmos um recurso que vai usar essa regra basta usarmos as `--tags`.

Para conectar no Windows, precisamos de um programa para o Remote Desktop. Ou podemos usar a extensão do Chrome para RDP.

Depois, precisamos setar a senha do usuário admin:

![](../media/windows3.png)