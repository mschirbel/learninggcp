# Cloud Endpoints

Com Cloud Endpoint podemos criar, monitorar, analisar e fazer deploy de uma API.
Uma API é uma interface para aplicações programáticas serem acessíveis.

Com Cloud Enpoint podemos construir a nossa API e expor a interface, mesmo com autorização, usando o Oauth 2.0

Tem alta disponibilidade, scaling e DDoS protection.