# Cloud Launcher

Podemos fazer o deploy de packages de software já preparadas. Usamos o Deployment Manager para saber informações sobre o deploy e o status do projeto.

![](../media/clauncher1.png)

Podemos ver vários exemplos de aplicações:

![](../media/clauncher2.png)

Podemos escolher as opções para as máquinas de deploy:

![](../media/clauncher3.png)

Após isso recebemos as informações:

![](../media/clauncher4.png)