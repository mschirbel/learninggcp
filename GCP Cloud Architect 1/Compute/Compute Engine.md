# Compute Engine

Compute Engine é um IaaS do GCP. Isso significa que a infraestrutura está totalmente invisível para o usuário, ou seja, não conseguimos ver os discos, os hacks e etc.

Para isso, podemos subir as VMs usando a infraestrutura do Google.

## Overview

Podemos criar instâncias, máquinas virtuais, rodando Linux ou Windows e podemos usar features de outros serviços:
    - networks
    - firewall
    - alb & nlb
    - subnetworks

As instâncias são rápidas para boot e foram consideradas número #1 como *price-performance*.

#### Costs

A conta do GCP funciona por base de minutos, no caso, podemos ser cobrados por base de 10min e temos discontos automáticos por deixar as máquinas ligadas por mais de um mês.

Não pagamos por instâncias pausadas, mas pagamos 100% do tempo de um disco, mesmo que esteja atrelado a uma instancia parada.

Podemos ver os detalhes do Custo, ao expandir o menu:

![](../media/vm4.png)
Esse custo só se aplica se a máquina estiver ligada.

### Create VM

Podemos criar máquinas com Windows e Linux, e com vários tipos de máquinas, com CPU, memória diferentes. Podemos fazer resize dos discos sem downtime e criar scripts para boot.

A primeira vez que você entra no Compute Engine, ele demora um pouco para tornar o serviço disponível pois ele tem que iniciar a API do Compute Engine.

Para criar uma VM clique em *Create* na box que está no meio da tela:

![](../media/vm1.png)

Podemos escolher várias opções para nossa instância:

![](../media/vm2.png)
E ao lado temos uma estimativa do custo mensal.

Quando a instância estiver criada, recebemos uma notificação no painel.
Podemos pausar, rebootar, iniciar ou deletar as instâncias, basta selecionar a checkbox e usar o menu acima:

![](../media/vm3.png)

Ao deletar uma instância, deletamos também o disco de boot.


### Options

#### Name
O nome da instância deve ser **único** no seu **projeto**. E deve seguir uma série de convenções do Google.

#### Zones
Podemos escolher diversas regions da GCP, mas lembre-se que quando criamos a instância ela fica presa aquela zone, não podemos movê-la. Para isso, teríamos que criar um snaphost e criar uma máquina a partir desse snapshot na zona desejada.

#### Machine Types
Podemos usar alguns tipos já pre-definidos de acordo com a necessidade da aplicação.

![](../media/vm5.png)
Para a conta Trial, temos uma limitações de CPU e Memória, que pode ser removido caso a conta seja do tipo Full.

#### Machine Size
Podemos também escolher valores customizados para Cores e Memória:
![](../media/vm6.png)

Assim podemos incluir GPU, também. E escolher qual GCPU vamos usar, mas isso depende de qual zona estamos.

Podemos escolher qual o tipo de processador também:
![](../media/vm7.png)
Isso pode ser restritivo a zones diferentes.

#### Boot Disk
Para o OS que vamos usar, temos as opções de usar alguns já disponíveis pelo GCP(OS Images), usar Application Images, Custom Images, Snaphosts e Existing Disks

![](../media/vm8.png)

Podemos escolher o tipo de disco, sendo Standard ou SSD
Algumas das imagens vem com custo de licenças mensais, que só é aplicada se a instância estiver ligada.

#### Identity and API access

Podemos escolher qual a service account que será usada. Se não temos nenhuma, usamos a default.

#### Firewall

Podemos escolher HTTP ou HTTPS. Podemos ter mais opções dentro do serviço de Network, aqui é somente HTTP ou HTTPS.

#### Management

Aqui podemos colocar labels, descrições e scripts de boot, bem como a parte de manutenção:

![](../media/vm9.png)

Também escolhemos a politica de deletion do disco:

![](../media/vm10.png)

Também alterar a parte de network interface.

![](../media/vm11.png)

E também até as SSH Keys, podemos criar ou usar uma custom.

![](../media/vm12.png)

e podemos criar a mesma instância usando o `gcloud`:
![](../media/vm13.png)

ou usando a API:
![](../media/vm14.png)

## Change Specifications

Para editar uma instância, basta clicar no nome e clicar no Botão Editar:

![](../media/vm15.png)

Nessa tela podemos ver algumas métricas, como CPU, disk e Network. E alguns detalhes da instância.
Podemos trocar o tamanho da instância, configurações de rede, configurações de disco, manutenções, services account.

Mas **não podemos** trocar a zone da máquina.

## Pre-emptible VM

É uma VM que dura pouco, e tem custo baixo(tipo uma spot na AWS). E tem um período de vida de no máximo 24h. É ótima para *short term batch processing*, ou seja, uma aplicação que tem Fault Tolerance e precisa de muito workload.

## Instance Template

É um template para definir o deploy de uma máquina no grupo(instance group) - tipo o Launch Configuration na AWS.

Definimos o boot disk, tipos de máquina, regra de firewall e IAM access.

## Instance Groups

São grupos de máquinas, com isso podemos controlá-las ao mesmo tempo. Podemos fazer o deploy de várias ao mesmo tempo. E existem 2 categorias

1. Unmanaged - as máquinas sao diferentes entre si e não temos auto scaling
2. Managed - podemos fazer o scaling das máquinas com base em métricas e temos ainda LB entre as máquinas. Se uma máquina falhar, ela é recriada.

![](../media/ig1.png)

E para todo instance group temos auto healing e auto scaling. Mas para isso precisamos do Instance Template. Com o auto scaling temos as seguintes opções:

![](../media/ig2.png)

Além das métricas acima, temos também o `Autohealing`, que mantém um health-check para ver se a VM está com algum erro.

O formato típico, são 4 dígitos aleatórios no final do nome das máquinas criadas pelo Instance Group. Há também uma opção de Rolling Update e Rolling Restart.