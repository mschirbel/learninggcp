# App Engine

App Engine é uma PaaS - Plataform as a Service. Funciona como uma plataforma que providencia um ambiente para desenvolvedores construirem suas aplicações sem se preocupar com a infraestrutura. Diferente do IaaS, que temos uma infraestrutura ao nosso dispor, numa PaaS, isso é focado no desenvolvimento de software.

App Engine é usado para construir apps e sites, sem se preocupar com infraestrutura. Funciona com vária linguagens:
    - nodejs
    - java
    - ruby
    - c#
    - go
    - python
    - php
    - .net

E podemos usar imagens Docker também. Mas o que necessariamente é administrado pela GCP?

1. firewall
2. DDoS
3. Patches
4. Virus
5. Network
6. Failover
7. Load Balancing
8. Scaling
9. OS Upgrade
10. Hardware fix
11. Certificates
12. Routing
13. IP/DNS

Ou seja, diferentemente do Compute Engine, que cuida de algumas coisas, o App Engine tem que cuidar de tudo isso para somente ficar pronto para o desenvolvedor criar o código da aplicação.

Existem dois tipos de ambientes: Standard e Flexible

Standard: Temos somente algumas versões de linguagens disponíveis e é um pouco mais rápido. Tem uma sandbox para o hardware e com algumas restrições.

Flexible: Baseado no Compute Engine, com scaling e balacing. Tem mais linguagens e a possibilidiade de usar Docker Image. A diferença é que somos cobrados pelo tipo de máquina, disco e etc.

App Engine é regional, ou seja, uma aplicação não pode ser acessada de outra region, por default.

## Deploy

Seguimos alguns passos simples para isso:
1. Clone do codigo
2. Revisão do código
3. Instalar os requisitos
4. Deploy

Para começar, acesse o serviço do App Engine:

![](../media/app1.png)

Depois, podemos abrir o cloud console e podemos usar o serviço do Google para isso:

![](../media/app2.png)

Criamos um novo referenciando o nosso código:

```
mkdir ourcode
gcloud source repos clone <NOME DO REPO Q VC CRIOU>
cd <NOME DO REPO Q VC CRIOU>
git pull https://github.com/GoogleCloudPlatformTraining/cpo200-guestbook
git push origin master
```

Dentro do serviço, podemos fazer a revisão do código:

![](../media/app3.png)

Para instalar os requisitos, isso depende da linguagem, em Python seria:

```
pip install -r requirements.txt -t lib
```

Para fazer o deploy, basta:

```
gcloud app deploy
```

Confirme a region e pronto:

![](../media/app4.png)

No dashboard, temos informações sobre o que acabamos de fazer o deploy:

![](../media/app5.png)

Quando clicamos, temos um dashboard com informações específicas:

![](../media/app6.png)

Dentro do menu Instances, sabemos quantas instâncias são usadas para suportar o aplicativo.
Em Services, podemos ver o acesso a determinadas URI da nossa app, quantidade de erros e tempo de resposta.

Dentro de Versions, podemos ter um histórico das versões de nossa app, e podemos controlar o tráfego entre essas versões:

![](../media/app7.png)

Podemos clicar em Split Traffic:

![](../media/app8.png)