# Snapshots & Custom Images

A semelhança dos dois é que com eles podemos criar novas instâncias(discos)

Mas a diferença é que snapshots são voltados para backups, e podemos fazer isso com as instâncias funcionando e tem um custo menor do que as imagens.
Os snapshots de um disco são incrementais, ou seja, após o primeiro snapshot, os outros serão somente da diferença. E eles ficam restritos ao projetos

Custom Images servem para criar novas instâncias. Mas o foco delas é ter a capacidade de voltar em um ponto no tempo da instância, esse estado de restauração pode ser compartilhando entre diversos projetos.


## Create Snapshot

![](../media/snap1.png)

O snapshot só usa o espaço que está ocupado no disco, o que não está sendo utilizado, não é usado no snapshot:

![](../media/snap2.png)

E se criarmos um segundo snapshot da mesma instância, vemos que o tamanho deles será diferente:

![](../media/snap3.png)

E se criarmos uma instância a partir do segundo snapshot, por baixo dos panos, ele aplica o primeiro e depois o segundo, somente as diferenças.

## Custom Image

Para criar uma imagem, precisamos pausar a instância. Clicamos em Images e clicamos em Create Image:

![](../media/snap4.png)

Inserimos as informações e o disco de fonte para a image:

![](../media/snap5.png)

Ao criar uma nova instância, basta selecionar qual a imagem deseja:

![](../media/snap6.png)