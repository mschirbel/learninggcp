# Disk Management

Dentro de Compute Engine, temos a seção de discos:

![](../media/disk1.png)

E perceba que o nome é o mesmo das instâncias, isso acontece porque são os discos de Boot das máquinas. Ao clicarmos em um disco, podemos ver seus detalhes:

![](../media/disk2.png)

Se clicarmos em Edit, podemos alterar o tamanho do disco. Isso acontece porque é um disco de rede, por isso ele está "preso" em uma Zone.

*Detalhe: podemos aumentar o tamanho do disco, mas não podemos diminuir.*
Os discos podem ir até 65TB.

Ao criarmos um novo disco, devemos dar algumas informações:

![](../media/disk3.png)

Uma das partes mais importantes é o **source type**. Podemos selecionar entre 3 tipos:
1. Image - assim criamos um *boot disk* mesmo que não esteja atrelado a nenhuma maquina
2. Snapshot - uma imagem de outro disco, que podemos usar para criar um clone de outra maquina ou um rollback
3. Blank Disk - sem software nenhum, apenas com o tamanho disponível.

A performance do disco é proporcional ao tamanho do disco. Maior o disco, melhor a performance.

Se fizermos um resize do disco, precisamos fazer o partition do novo espaço do disco. Entretanto, podemos fazer um reboot da máquina para fazer isso automaticamente.

Ou podemos fazer a partition diretamente no SO. 

Algo que podemos trocar, é quando criamos uma instância, podemos selecionar a opção de manter o disco após deletar a instância. Isso pode servir como um backup final da instância.