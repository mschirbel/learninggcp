# Compute Options

Temos algumas formas de sustentar as aplicações em GCP:
1. Google Compute Engine
2. Google App Engine
3. Google Container Engine
4. Google Cloud Functions

Com isso, temos as funcionalidades de storage, network, security e big data, integradas em fácil escala.

![](../media/compute-flex.png)
*Firebase é a plataforma de desenvolvimento mobile*.

Mas podemos escolher vários dessas plataformas para sustentar todos os tiers de nossa app.

## Compute Engine

É o IaaS, onde podemos criar máquinas virtuais, chamadas de *instances*. Podemos configurar:
- cpu/gpu
- memory
- dh
- OS
- firewall
- network

Isso tem muita flexibilidade, mas também traz overhead para o adm.

Para escolher, podemos usar a árvore de decisão do Google:

![](../media/compute-tree.png)

## Container Engine

É usado para fazer o deploy de containers. É um K8s por baixo dos panos, e podemos usar nossa aplicação independente do OS.

## App Engine

É um PaaS. Os devs criam o código e o Google cria a infraestrutura. Por isso, nunca tocamos a infraestrutura. Mas podemos fazer deploys e escalar de forma automática.

Existem dois tipos de ambientes: *Standard* e *Flexible*

Temos suporte para o Standard em:
1. Python
2. Java
3. PHP
4. Go

Temos suporte para o Flexible em:
1. Java 8
2. Servlet 3.1
3. Jetty 9
4. Python 2.7
5. Python 3.5
6. Nodejs
7. Ruby
8. PHP
9. .NET core
10. Go
11. Podemos usar também o runtime com uma Docker Image

## Cloud Functions

É o Serverless do GCP. Podemos usar com *event-driven*, ou seja, a função funciona como um trigger baseado em um evento da nuvem. Devem ser escritas em Javascript e executadas em Nodejs.