# Network Overview

## VPC

São a fundação para as redes em nuvem. São uma versão virtual das redes físicas na nuvem. Temos conectividade com as intâncias, containers e outros recursos da GCP.

Todos os projetos da GCP começam com uma rede chamada *default*. Os recursos dentro dessa rede tem uma IP interno baseado na subnet em que estão. Todos os outros recursos estão atrelados a uma VPC

## External IP address

Uma instância, por exemplo, podem ter 1 ou mais interfaces de rede, e cada uma delas tem um IP. Que podem ser interno, externo ou ambos.

External IP's são efêmeros ou estáticos. Os efêmeros podem mudar caso a instância seja restartada.
Os estáticos se mantém a uma intância.

## Firewall Rules

Toda a VPC tem um conjunto de regras de firewall. Podemos controlar o *inbound* e o *outbound*, que podem ser aplicados para uma VPC ou somente para recursos

## Routes

É uma rota de um ponto A para um ponto B. Marcamos um range de IP para um destino. Quando criamos uma VPC, sempre temos uma routing table por default

## Load Balancing

Podemos distribuir acessos entre recursos, ou usar em conjunto com Instance Groups.

## Cloud DNS

DNS é traduzir um nome de site para um IP. O Cloud DNS é global, resiliênte e altamente performático. Podemos nele criar *managed zones*.

## Cloud VPN

Podemos interagir com uma VPC de um ambiente *on-prem*, usando ou a internet ou uma conexão criptografada. Assim o GCP VPN só suporta conexão de gateway para gateway, não podemos usar um client para conectar.

## Cloud CDN

CDN publica o conteúdo perto do usuário para melhorar o tempo de resposta. O conteúdo fica em cache em Edge Cache Sites em todo o mundo.