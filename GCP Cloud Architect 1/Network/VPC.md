# VPC

É uma versão virtual de uma rede física que existe na GCP. Qualquer recurso pode ser adicionado a uma VPC. Podemos ter uma ideia de como seria uma rede prevada na GCP:

![](../media/vpc1.png)

Para criar um VPC, vamos na aba de VPC Networking dentro do menu principal:

![](../media/vpc2.png)

Temos a VPC default, que é criadas assim que iniciamos o projeto. E dentro dela temos várias subnets, uma para cada region. Cada subnet tem seu CIDR.

Para criar uma nova VPC, clicamos em *Create New VPC*.

![](../media/vpc3.png)

Dentro ja podemos criar nossas subnets, seja de forma *custom* ou *automatic*, que é a GCP colocando o CIDR para nós. Caso seja automático, podemos depois criar novas subnets, mas não poderemos deletar as que foram criadas automaticamente.
Depois podemos importar nossas *firewall rules*. Seja da VPC default ou de outras.

Uma vez que trocamos para o modo *Custom* da VPC, não podemos voltar para o automático.

Dentro da VPC, quando criamos, temos a opção de marcar *Private Google Access*, que permite que VMs acessem outros serviços do Google sem usar um IP externo.

Dentro da VPC podemos selecionar quais *Routes* teremos e também os VPC Peerings:

![](../media/vpc4.png)

Dentro do menu da criação da VM, temos opções para nossa custom VPC:

![](../media/vpc5.png)