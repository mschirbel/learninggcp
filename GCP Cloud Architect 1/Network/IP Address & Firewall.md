# IP Adress

Todas as instâncias tem um IP privado, com esse IP usamos para comunicar com outros serviços. Mas podemos ter um IP público, que pode ser de duas formas:
    - efêmero: troca quando a instância para
    - estático: reservado e atrelado a uma instância

Instâncias só podem ter 1 IP público e se for um estático é cobrado $0.01/hour

# Firewall Rules

Seja em redes em nuvem ou não, regras de firewall servem para proteger tráfego *inbound*(ingress) e *outbound*(egress).

São regras de *allow* ou *deny*. São baseadas em IP, porta e protocolos. E podem ser aplicadas em recursos ou em toda uma VPC.

---

## Assign Static IP

Podemos inserir um IP estático na aba de Networking:

![](../media/vpc6.png)

Basta dar um nome e uma descrição:

![](../media/vpc7.png)

No serviço de External IP Adressess, podemos ver todos os IPs públicos que temos, sejame estáticos ou efêmeros, e podemos desatrelar um estático de um recuros e atrelar a outro:

![](../media/vpc8.png)

Podemos criar um Reserved Static, assim como criamos direto na instância:

![](../media/vpc9.png)

--- 

## Creating Firewall Rules

Para criar uma Firewall Rule:

![](../media/firewall1.png)

Essa regra deve ter uma número de prioridade, caso haja conflito assim será decidido. Quanto menor o número, maior a prioridade.

Depois podemos selecionar quais serviços vamos ter nossa Rule:

![](../media/firewall2.png)

Com tags, podemos selecionar quais recursos vao receber nossa regra, é assim que é feito a seleção na regra.