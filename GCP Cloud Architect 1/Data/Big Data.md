# Big Data

Big Data é a enorme quantidade de dados que pode ser analisada, processada, associada para coletar padrões e correlações entre interações humanas.

Big data é custoso, pois envolve muito processamento e storage. Fora que, os processamentos podem demorar muito, caso sejam mal-feitos.

Para isso, surgiram tecnologias para melhorar isso, como Apache Hadoop e NoSQL DB.

Entretanto, agora as empresas estão coletando mais dados, e agora mais do que nunca, com o IoT, o que aumenta muito mais o compliance entre os dados.

Logo, Big Data pode trazer valor a um negócio, algo que pode traçar um plano para sair a frente dos outros.

E a Cloud é a principal maneira de cuidar de tecnologias de BigData, pois ela facilita a complexidade das ferramentas e deixa o processo mais automatizado.

## GCP Services

- Google BigQuery
- Google Cloud Dataflow
- Google Cloud Dataproc
- Google Cloud Datalab
- Google Cloud Dataprep
- Google Cloud Pub/Sub

Para a prova, é interessante saber como cada um funciona e como relacioá-los.

#### BigQuery

Usa SQL para fazer queries em quantidades massivas de dados, assim, podemos fazer análises em tempo real e o processamento é feito pela GCP.

#### Dataflow

Funciona como um pipeline de dados para outros serviços, podendo fazer batch e stream processing.
Batch  => pegar um grande volume de dados de uma vez só processa
Stream => temos dados entrando constantemente e o processamento é feito constantemente

É Opensource e se integra com outros serviços da GCP

#### Dataproc

É como usar o Apache Spark e Apache Hadoop cluster.
Assim podemos escalar o cluster rapidamente com recursos cobrados por minuto.

Podemos usar para fazer migrações para a cloud e usar em serviços de Machine Learning

#### Datalab

É um Jupyter Notebook online, que podemos usar para fazer exploração, análise e visualização de dados.

#### Dataprep

É serverless, roda em cima do Dataflow e prepara dados para visualização e análises

#### Pub/Sub

É um middleware de mensagem, assim, podemos ter `topics` para entregar dados em serviços da GCP. Assim podemos definir uma stream de dados, para enviar dados para outros serviços.

É um correio de dados, basicamente.

## Lifecycle

![](../media/data1.png)

Ou um exemplo de como podemos integrar com serviços externos:

![](../media/data2.png)