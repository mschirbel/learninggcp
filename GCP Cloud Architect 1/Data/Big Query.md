# Big Query

Existe uma organização dentro do Big Query. 
1. Projects - o mesmo que no GCP, e podem ser compartilhados
2. Datasets - conjunto de tabelas, onde podemos ter o nivel mais granular de acesso
3. Tables   - assim como no Excel, onde os dados estão em colunas e linhas
4. Jobs     - requests para dados

![](../media/bigquery1.png)

Podemos acessar do menu principal:

![](../media/bigquery2.png)

E assim teremos acesso a interface do Big Query que foge um pouco do Padrão. Podemos aqui criar nossos projetos e datasets:

![](../media/bigquery3.png)

Assim, vemos que temos até datasets públicos, que podem ser usados para testes.
Toda a query que fazemos dentro do BigQuery é feita em SQL.

Vemos que algumas dessas tabelas nunca poderiam ser tratadas por um Excel:

![](../media/bigquery4.png)

Toda vez que entramos com uma query em SQL, recebemos um validador de sintaxe:

![](../media/bigquery5.png)
E o retorno é realmente muito rápido. O custo é feito em cima de cada uma das consultas.

Podemos criar uma tabela, seja manualmente ou pegando de vários locais:
![](../media/bigquery6.png)