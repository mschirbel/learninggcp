# Machine Learning

ML é a capacidade de aplicações se adaptarem e aprenderem como contornar situações para as quais não foram explicitamente programadas para.
Isso é importante para as fronteira de previsão de dados, principalmente quando usamos grande quantidade de dados.
Diversas aplicações já usam, como assistentes virtuais, traduções, identificação de imagens e carros autônomos.

### TensorFlow

É uma ferramenta open source do Google que serve para construir modelos e redes neurais. Foi usada dentro do Google antes de ser distribuída para o mundo. Assim como o Kubernetes.

## ML Engine

É um serviço que permite criar nosso serviço de ML, assim podemos analisar qualquer estilo de dados, em qualquer tamanho.
Ele se integra com diversos outros serviços e pode ser usado para análises preditivas.

## Cloud Vision API

É um serviço de reconhecimento de imagens, assim classificando imagens em diversos tipos de categorias, sejam eventos, pessoas, lugares ou até conteúdo inapropriado.

## Cloud Natural Language API

Podemos usar esse serviço para extrair informações de textos, ou seus significados. Assim podemos entender o sentimento atrás de um texto, ou de uma quantidade enorme de textos.

## Cloud Translate API

Podemos traduzir linguas, mesmo que não sabemos qual a fonte da língua.

## Cloud Speech API

Podemos traduzir audio em texto ou vice versa. Suporta até 110 línguas e tem noção do contexto ao qual o texto está inserido transferindo isso para a linguagem.

## Cloud Video Intelligence

Consegue identificar em vídeos, o conteúdo, buscando por objetos, pessoas e detect transições de cena.
Isso é útil para uma quantidade enorme de dados em vídeo.