# Routes

Toda as rotas feitas na rede da GCP são feitas em software, não em hardware. Por default temos algumas rotas já criadas para informar o que sai/entra das VM's.

Rotas e Firewall Rules determinam aquilo que pode entrar e sair das instâncias.

Para criar uma route, temos que selecionar um nome e um IP range de destino:

![](../media/route1.png)

A prioridade funciona com base em maior procedência para números menores, sendo 1000 o default.
Assim, podemos aplicar essa route para uma network tag. Se não, podemos somente colocar em toda a rede.

Se colocarmos o destino como uma instância, precisamos ter um IpV4 Forwarding.