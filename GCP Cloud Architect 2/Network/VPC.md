# VPC

É uma representação virtual de nossa rede. VPC são SDN(Software-Defined Network) que são a fundação de todas as outras funções de rede na GCP.  

Assim, temos os conceitos virtuais ainda aplicados, como Firewalls, Routes, Load Balancing, DNS e etc.  

E ainda podem ser extensões de uma rede híbrida.

## Limits

Existe um limite de 7000 VM por VPC e só temos unicast para IPv4, não temos multicast.

## Shared VPC

Por default, uma VPC está atrelado a um único projeto, entretanto, podemos ter a necessidade de ter uma rede que vá por vários projetos.

O nome disso é Cross-Project-Networking, antigamente, ou como chamado na GCP, Shared VPC.

Precisamos de algumas coisas:
- host project - o projeto que vai criar a vpc shared
- service project - o projeto que vai poder usar e tem permissoes para usar a VPC
- shared vpc admin - é uma iam role para ser o admin da VPC
- service project admin - que é o admin da vpc compartilhada no service project

Uma shared vpc só funciona dentro de uma cloud organization. E o service project só pode usar 1 shared vpc, e não podem ser o mesmo projeto.

E isso é bom para podermos dividir os projetos, inclusive os billings, em associações de fácil uso para outras áreas.

Todas as iam roles devem ser dadas no nível da organiação, pois se estendem para múltiplos projetos. Isso acontece porque podemos escolher somente algumas subnets para estarem compartilhadas, ou até mesmo toda a VPC.