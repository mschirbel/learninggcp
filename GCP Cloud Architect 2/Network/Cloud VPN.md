# Cloud VPN

Um exemplo de como o Cloud VPN pode funcionar é o seguinte:

![](../media/vpn2.png)

Mas uma coisa interessante, é que se as nossas instâncias tem acesso na internet, poderemos fazer o acesso nas outras máquinas via IP Externo, mas não conseguimos fazer pelo IP interno, que é o que gostaríamos.  
Para fazer isso, precisamos dos nossos VPN Gateway.  

Para acessá-lo vá em:

![](../media/vpn3.png)

Agora podemos criar o Gateway. Primeiro marcamos as informações da VPC origem:
![](../media/vpn4.png)

*Repare que estamos usando um IP Static, para não haver problemas de efemeridade.*

Depois, vamos criar os Tunnels, que é a forma como diremos para o gateway como ele deve se comunicar do outro lado:

![](../media/vpn5.png)

E para isso também precisamos de um IP estático, para isso que criamos conforme abaixo:

![](../media/vpn6.png).

Também é necessário de uma senha para o Tunnel. E em *Remote Network IP ranges* devemos colocar o CIDR das subnets dentro da VPC.

Mas mesmo depois disso ainda precisamos fazer liberação de firewall rules.