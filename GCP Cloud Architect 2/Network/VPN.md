# VPN Overview

Existem algumas opções para conectar uma rede on-prem com a rede da GCP:
1. cloud interconnect
2. dedicated interconnect
3. peering
4. cloud vpn

## Dedicated Interconnect

É uma conexão física de fibra ótica que vai diretamente dos servidores on-prem para uma Edge Location.
Assim, a VPC pode assumir IPs internos da rede da empresa. Isso é bom para grandes corporações com ambientes híbridos(on prem e cloud) ou quando precisamos de uma banda larga bem rápida.

Uma rede ficaria mais ou menos assim:

![](../media/vpn1.png)

## Peering

Peering é uma forma de conectar duas redes usando uma conexão direta com a rede do Google, sem usar a Internet. Isso é bom quando precisamos de acesso aos serviços do Google mais rápido, não somente a GCP.

## Cloud VPN

É uma conexão com a VPC da GCP com o on-prem usando um tunnel criptografado sob a internet. Podemos usar diversos Tunnels para melhorar a performance e ter mais disponibilidade. Podemos usar rotas estáticas e dinâmicas.