# Firewall

Só existe um único firewall em toda a VPC. Com isso temos as regras de entrada para as instâncias. Podemos criar regras de inbound/outbound.

Temos um *implicit deny all* ingress.  
Temos um *implicit allow all* egress.  

As regras mostram o destino, protocolo, ação e prioridade para as entradas e saídas.

Existem algumas regras que são criadas por default:

![](../media/firewall1.png)

Perceba que em uma regra temos os *targets* e os *source filters*.
Para que uma instância receba essa regra, ela precisa ter uma tag com um match nos *targets*. E os source filters servem para filtrar por subnet, ou até por IP.

Quando criamos uma regra, temos que especificar seus targets, podendo ser:
- uma tag
- uma service account
- todas as instâncias na rede

![](../media/firewall2.png)

E para usar uma tag na instância, devemos editá-la e em Network Tags colocar a tag que criamos:

![](../media/firewall3.png)

Temos também o *Second Source Filter*, que funciona com um **OR** com o primeiro filtro, seria ou um ou outro. Por últimos, mostramos quais protocolos e portas queremos liberar.

## Command Line

Para criar uma Firewall Rule podemos usar:

``` 
gcloud compute firewall-rules create <NAME> --allow=<PROTOCOL>:<PORT> --direction=<INGRESS|EGRESS> --priority<NUMBER> --network=<NETWORK> --source-ranges=<IP> --target-tags=<TAG>
```

Podemos ver o comando, quando criamos a rule via console também. Para adicionar em uma instância:

```
gcloud compute instances add-tags <NAME> --tags <TAG>
```

