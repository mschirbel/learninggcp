# Overview

Todas as regions da GCP estão em uma rede privada do Google. Isso porque todos os pontos espalhados são comprados pelo Google. Ou seja, podemos pingar todas as instâncias usando nosso IP privado.

## SDN

Software Defined Networking. É um jeito de administrar os switches, routers, LB, firewall e storages. Ao invés de fazer isso presencialmente, podemos fazer usando um programa. Os conceitos fundamentais de redes também se aplicam, como subnets, routes, regras de firewall, DNS e etc.

Quando tratamos da VPC, só temos em escala global. A VPC default é global, está em todas as redes do mundo. Isso significa que todos os recursos da VPC também são globais - dns, elb, subnets, firewall rules.

Um exemplo de rede pode ser:

![](../media/network1.png)

