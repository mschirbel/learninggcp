# Cloud Router

Com um Cloud Router podemos fazer nossas subnets terem IPs dinâmicos.
Para mostrar como um roteamento dinâmico funciona, vamos fazer o seguinte:

![](../media/router1.png)

Aqui temos duas novas nomenclaturas:

1. ASN - autonomous system number: é um número id para saber diferenciar um router de outro
2. BGP - border gateway protocol: é um IP privado para identificar um router na rede

Para usar o dynamic routing, precisamos alterar o modo da VPC de routing para *global*:

![](../media/router2.png)

Depois entramos no Interconnect e vamos em Cloud Routers:

![](../media/router3.png)
Criamos os Routers referenciando a region e o ASN respectivo.

Agora, basta criar um Cloud VPN para as duas vpc, mas dessa vez usando um Routing dinâmico:

![](../media/router4.png)

E em BGP session, preenchemos com informações dos dois routers que vamos criar:

![](../media/router5.png)

Agora nossos routers descobrem novas subnets sem precisar configurarmos conexões estáticas entre elas.