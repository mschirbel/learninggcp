# Google Container

GKE e GAE são exemplos de infraestruturas gerenciadas pela GCP, ou seja, não precisamos nos preocupar com problemas de segurança, atualizações na infraestrutura em si.

No caso do GKE, podemos criar clusters de Kubernetes com pouco esforço.

Mas temos alguns outros serviços quem podem complementar o GKE, como por exemplo:
- Container Build - para criar as imagens a partir de um arquivo de configuração
- Container Registry - para armazenar as imagens

## Container Build

Podemos usar codigos importados do Github, Bitbuckets, GC Storage.
Essas imagens geradas sao automaticamente guardadas no Container Registry,

## Container Registry

É um repositório privado de imagens Docker. Consegue se integrar com vários sistemas de CI/CD para fazer ``push`` e `pull`.

## GKE

É o serviço que fazer a administração de um cluster de Kubernetes.

![](../media/gke1.png)

Temos diversas opções para personalizar nosso cluster. Incluindo um autoscaling, que são gerenciados pela GCP, com métricas próprias.

A partir da criação do cluster, podemos adicionar um `node pool`, que são máquinas configuradas de formas diferentes, mas ainda linkadas com o cluster original.