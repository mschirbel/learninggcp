# Overview

IAM é um serviço que determina quem, pode faze o que, em qual recurso.
Para isso, temos os:

**Members**: que são pessoas, com e-mail do GSuite ou de algum domínio. Também podem ser uma application(service account)

**Roles**: Uma coleção de permissões, assim podemos atrelar uma Role a um Usuário, e nunca um usuário diretamente a permissão.

## Roles

Temos dois tipos:

1. Primitive - estão a nível de projeto, e podem ser:
    - Viewer
    - Editor
    - Owner

2. Predifined - são mais granulares e podemos colocar a nível de recurso.

## IAM Policy

É uma coleção que define um acesso, e podemos colocar várias policies em uma role

## Policy Hierarchy

Uma policy pai pode ter herança em uma policy filho. Assim podemos usar um formato restritivo de policies.

## Create Folder

Para criar uma folder, precisamos:
- estar no nível de organization
- ter a role de folder administrator

E temos que ter em mente que **todas as permissões são baseadas no e-mail** que a pessoa fez login no Google.
E podemos ver que podemos ter múltiplas roles para um usuário:

![](../media/iam1.png)

## Using GShell

Para usar a linha de comando, precisamos passar um arquivo `JSON` com a policy. Para vermos todas as configurações de um projeto:

```
gcloud projects get-iam-policy <PROJECT ID> --format json > iam.json
```

Para alterar isso, devemos alterar o `JSON`. E depois submetê-lo:

```
gcloud projects set-iam-policy <PROJECT ID> iam.json
```

As roles que já existem criadas pelo Google, não podem ser modificadas. Mas podemos usar elas como templates para criar as nossas:

![](../media/iam2.png)