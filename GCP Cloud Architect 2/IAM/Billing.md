# Billing

Precisamos ser informados de como as nossas contas ficam conforme crescemos com a infraestrutura.
E Temos roles para isso no IAM:

- billing account creator
- billing account administrator
- billing account user
- billing account viewer
- project billing manager

E todas as Billing Accounts estão ligadas a um projeto.

---

Depois, podemos exportar todos os dados para um Cloud Storage ou para o Big Query. Entretanto, diretamente no console podemos habilitar alertas para o consumo.
Desde que sejamos administradores do Billing, podemos ver todos os recursos envolvidos na conta.

Podemos tomar algumas ações:

![](../media/billing1.png)

Podemos trocar o nome e fechar a conta, mas se fecharmos a conta todos os recursos vão parar de funcionar.

## Alarms

Para criar um alarme clique em *Budgets & Alerts*:

![](../media/billing2.png)

Os alarms vão ser enviados para os administradores do IAM. Pelo e-mail do Google.

## Transactions

Aqui podemos em quais serviços estamos gastando dinheiro:

![](../media/billing3.png)

Vemos os débitos e créditos que recebemos.

## Export

Podemos exportar os dados para dois locais:
1. Big Query - com um dataset, para termos uma tabela com os dados do billing
2. CSV - temos um arquivo que pode ser usado programaticamente ou importado em outras ferramentas.

![](../media/billing4.png)

No BigQuery podemos encontrar dados com query de sql:

```sql
SELECT product, resource_type, start_time, end_time, cost, project_name, currency, usage_amount
FROM [nome-do-projeto]
WHERE {[nome-do-projeto] > 3}
```

## Billing with IAM

Sempre que criarmos um novo projeto, dentro de uma organização, precisamos conceder a Role de Billing Administrator, de dentro da conta da Organization.

![](../media/billing5.png)

1. Administrator - tem todas as permissões com Billing
2. Creator - somente pode criar
3. User - pode associar projetos a billing
4. Viewer - pode somente ver os dados do Billing
5. Pode asssociar e desabilitar billing accounts