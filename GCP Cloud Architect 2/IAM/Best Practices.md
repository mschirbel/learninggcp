# Best Practices

## Least Privilege

Os membros devem ter somente a permissão para fazer o necessário. E para isso, devemos criar nossas próprias roles, pois assim temos mais controle do que o Google sobre a organização.

## Separate Service Account

Devemos usar uma service account para cada microserviço, ou aplicação.
E elas devem ser restritivas. Mesmo a nível de herança.

## Care with the Owner

Não devemos usar(talvez) a role de Owner, mas talvez a de Editor seja suficiente.

## Rotate Keys

Rotação de Keys para o Service Account, quando são controladas pelo usuário. As do Google são rotacionadas automaticamente.

E não devemos usar as keys dentro do código.

## Auditing

Podemos exportar as Logs para um bucket para fazer análises.

## Use Groups

Grupos podem encurtar o trabalho com múltiplos usuários.