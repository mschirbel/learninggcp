# Service Account

Não precisam de uma autenticação de um usuário humano, por isso, serve para autorizações para aplicações e serviços. Assim os serviços podem ser comunicar. E são identificadas por um e-mail da organização.

## Tipos

#### Google Managed

Representadas por diversos serviços do Google. E tem o formato `<PROJECT_NUMBER>@cloudservices.gserviceaccount.com`

Possuem **keys** controladas pelo Google, não temos a opção de mudar.

#### User Managed

Podemos criar um, baseados em um projeto, podendo ser de dois formatos:
`<PROJECT_NUMBER>-compute@developer.gserviceaccount.com`
`<PROJECT_ID>@appspot.gserviceaccount.com`

Podemos criar e administrar as nossas **keys**. O Google guarda a chave pública, mas nós temos que controlar a privada.

## Diferenças

Service Account são tanto o membro quanto o recurso de uma Role.
![](../media/iam3.png)

Uma service account usa uma **account key** para fazer a autenticação.

## Scopes

É um método legado. Com scopes podemos dar acesso por recurso:

![](../media/iam4.png)

Mas ainda funcionam, e uma autorização total considera ele também.
Para testar a service account de dentro de uma instância:

```
gcloud config list
```