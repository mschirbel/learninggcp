# Management Tools for Development

## Cloud Source Repository

É um repositório Git hosteado na GCP, podemos fazer algumas integrações com o Stackdriver para fazer alguns debugs. Mas a parte importante, é lembrar que ele faz para de um git flow com serviços somente na GCP.

## GAE Management

Dentro do App Engine podemos usar serviços da GCP para melhorar o desenvolvimento de aplicações, como por exemplo:
- usar o cloud shell para debug
- usar versionamento com split traffic
- usar firewall rules para IPs específicos
- blue/green deployments
- break app into microservices
