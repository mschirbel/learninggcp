# Organization & Nodes

![](../media/organization.png)

## Organization

Uma organização é o level mais alto para qualquer recurso da GCP, é como se fosse um endereço de e-mail de uma empresa, por exemplo. Podendo usar, por exemplo, um e-mail do Gmail, mas não um que seja pessoal. Deve ser um e-mail corporativo, caso contrário, será uma conta de uso pessoal.

Logo, podemos ter o e-mail corporativo de duas formas
1. GSuite
2. Cloud Identity Domains

Com essa organização podemos dar acesso a pessoas diferentes dentro dos projetos e da organização. E lembrando, que só existe uma única organização.

Existem 2 tipos de usuários dentro de uma organização:
1. Organization Admin - ele tem controle total. É bom para auditoria. Podem haver vários
2. Organization Owner -  podem haver vários, e eles tem poder relativo aos seus acessos.

## Folders

Podemos ter projetos dentro de uma *folder*, pois assim os recursos desse grupo dividem permissões de IAM. Podemos garantir uma role para uma folder, e assim ter todos os recursos dentro dela dividindo os acessos.

Mas só podemos criar uma folder dentro de uma conta que seja uma organization:

![](../media/folders1.png)

Dentro de IAM -> Manage Resources