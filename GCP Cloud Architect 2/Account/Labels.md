# Labels

Labels são usados para organizar recursos, em um nível mais granular do que em projetos e organizações.
Labels são como tags(mas são diferentes), nas quais podemos usar para agrupar recursos.

É um key-value pair:

```
Key: Value
```

A `key` não pode ser vazia, mas o `value` pode. Depois, podemos agrupar as Labels para saber custos de recursos, podemos ter alguns casos de uso, por exemplo:
- environment
- owner
- team
- app
- cost center
- resource state

## Diferença para Tags

Labels | Tags
:------ | ----:
Podem ser usadas em toda a GCP | Somente para Network/VPC
Não afeta os recursos, somente para identificação | Afeta os recursos, por exemplo uma regra de firewall

## Create Resource with Labels

Podemos colocar Labels quando estamos criando uma instância:

![](../media/labels1.png)

Ou podemos usar o `gcloud`:

```
gcloud compute instances create instance-x --labels contact=Marcelo,state=inuse
```

Para adicionar a uma instância que já existe, basta editar a instância ou selecionar múltiplas:

![](../media/labels2.png)

Para trocar via `gcloud`:

```
gcloud compute instances update <INSTANCE NAME> --update-labels contact=Marcelo
```

E podemos pesquisar por elas:

![](../media/labels3.png)