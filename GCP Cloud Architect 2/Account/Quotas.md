# Quotas

São limites que podemos impor aos recursos que criamos, por exemplo:
1. 48 CPU total por região e 5 IP's por projeto

Isso pode ser útil para evitar gastos desnecessários, e podemos impor em:
- recursos de projetos
- api request de projetos
- por region

Uma das melhores coisas da Cloud é que temos a elasticidade e escalabilidade sempre a mão, então usar Quotas podemos evitar consumos desnecessários e custos elevados no fim das contas.

Para ver as quotas podemos ir em IAM -> Quotas

![](../media/quotas.png)

Podemos filtrar por serviços, métricas ou regions:

![](../media/quotas2.png)

Para soliticar um aumento, selecionamos qual o limite queremos editar, e clicamos em Edit Quotas:

![](../media/quotas3.png)