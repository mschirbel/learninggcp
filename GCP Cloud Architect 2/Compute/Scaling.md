# Scaling

Podemos realizar certas automações em nossas instâncias, geralmente são trabalhos repetitivos e documentados, para que possamos criar um script/código que faça isso para nós.

E uma das funções mais interessantes de Cloud, é a elasticidade de mais capacidade. Isso reduz a complexidade e melhoramos a arquitetura do sistema.

Existem três componentes essenciais para o Scaling:

- Load Balancer
- Instance Group
- Autoscaling

## Load Balancer

O LB funciona como um único ponto de entrada para um conjunto de servidores que cuidam da aplicação. Ele divide a carga de entrada entre essas múltiplas máquinas.

Existem três tipos de LB:

1. Global External LB
Para isso temos os HTTP e SSL/TCP Proxy, eles são públicos e podem ser acessados globalmente. Isso também serve para acessos globais, pois serve para a infra mais próxima da requisição.
Ou pode fazer o tráfego viajar via conteúdo, como sites que tem línguas diferentes na interface.

Para isso, usamos um *Forwarding Rule*, que faz com que o tráfego vá para um Target Pool, que pode ser um Instance Group(mas não é obrigatório).

![](../media/lb1.png)

![](../media/lb2.png)

2. Network (External) Load Balancer
Ele funciona para múltiplas VPC
É feito para protocolos que não são HTTPs. Ele faz o balance para IPs. Para ele também temos *Forwarding Rule* indo para um Target Pool.

3. Network (Internal) Load Balancer
É parecido com o número 2, mas só funciona para a VPC.
Geralmente é usado para *multi-tier* apps.

---

Temos um exemplo do uso dos LB abaixo:

![](../media/lb3.png)


# Load Balancing

Fica na parte de `Network services`. Podemos escolher entre três tipos:
- HTTP(s) LB - na camada 7 - pode ser publico e multi region
- TCP LB - na camada 4 - pode ser publico e single-region
- UDP LB - na camada 4 - pode ser publico e single-region

![](../media/ig3.png)

Podemos ter dois tipos de backends:

- Instance Group
- Buckets

![](../media/ig4.png)

Podemos ter mais de um tipo de backend em um LB. Basta configurar em `Host and Path Rules`.

Para o frontend, escolhemos se sera HTTP ou HTTPS e se teremos algum IP reservado.

# Canary Testing

Ao selecionarmos o Rolling Update, podemos colocar mais um template para que o Instance Group saiba a porcentagem que deve, temporariamente, criar.

# Cloud Deployment Manager

É um serviço para gerenciamento de deploys de recursos da GCP. Podemos ter arquivos de configuração e templates para isso.

Para isso, usa-se APIs que criam os recursos. Como o Cloudformation na AWS.

Os arquivos de configurações são feitos em YAML, e os arquivos template podem ser em Python ou em Jinja2. Após o uso, é gerado um output chamado de manifesto. Um arquivo somente de leitura.

Podemos ver [o repositorio oficial](https://github.com/GoogleCloudPlatform/deploymentmanager-samples/tree/master/examples/v2) com diversos exemplos.

Dentro do `cloud shell`, podemos fazer o deploy com o comando:

```
gcloud deployment-manager deployments create <NAME> --config <FILE>.yaml
```