# Disks

Os discos são partes essenciais para persistência de dados em cloud. Existem alguns tipos de storage no GCP:

![](../media/disks1.png).

Para as instâncias, só podemos usar um Persistent Disk para o Boot Disk. Os outros dois tipos, podemos usar nas instâncias como um disco auxiliar.

## Persistent Disk

É a opção default quando criamos uma instância. É durável e confiável, isso se deve porque não está alocado fisicamente com a máquina. Ele é um disco virtual, e está na mesma rede que a instância.

Isso nos permite desalocar um disco de uma instância(desde que não seja o boot) e colocar em outra. Assim, os discos podem ser movidos e flexibilizados conforme a necessidade.

A performance está atrelada com o tamanho do disco, quanto maior o disco, melhor a performance.

Caso precisemos fazer uma partition, podemos simplesmente adicionar um novo disco, que ainda assim, pode ser criptografado. 

## Local SSD

É um disco mais rápido que o Persistent Disk, mas não pode ser um disco de Boot. O disco é deletado juntamente com a instância.
Um outro ponto de atenção é que sempre tem 375GB, não configurável. Podemos ter até 8 discos em uma instância.

## Cloud Storage Bucket

Não pode ser um boot disk, mas temos um acesso global nele, sendo criptografado ou não.
A flexibilidade deve ser considerada pela perda de performance.

---

## Métricas

![](../media/disks2.png)

## Performance

![](../media/disks3.png)

## Costs

![](../media/disks4.png)

---

## Boot Disk

Podemos usar alguns tipos de tipos de Boot Disk:

![](../media/disks5.png)
Além disso, podemos extender os discos:

![](../media/disks6.png)

**Esses discos precisam estar na mesma zona do que a instância que estamos criando.**

Quando estamos criando um tipo de disco, podemos escolher alguma base:
- imagem(algum SO)
- snaphost(algum snapshot de outro disco)
- nada(podemos criar um disco em branco)

Podemos criar via gcloud:

```
gcloud compute disks create disk1 --size 50GB 00type pd-ssd --zone us-east1-a
```

## Manipulate Disks

Podemos adicionar um disco a uma instância e depois forçar o SO a reconhecê-lo e aumentar o espaço vago.
Por exemplo, podemos fazer via console, editando a instância:

![](../media/disks7.png)

Ou podemos fazer via gcloud:

```
gcloud compute instances attach-disk <NOME DA INSTANCIA> --disk <NOME DO DISCO> --zone <zone da instancia>
```

Para o linux podemos colocar um novo disco no FS, temos que:
1. formatar o disco(isso apaga td no disco)
2. criar o diretório de mount
3. montar o disco no diretório
4. dar as permissoes corretas

```s
sudo su
lsblk
mkfs ext4 -m 0 -F -E lazy_itable_init=0, lazy_journal_init=0, discard /dev/sdb
mkdir -p /mnt/disks/disk2
mount -o discard,default /dev/sdb /mnt/disks/disk2
chmod a+w /mnt/disks/
lsblk
```

Para o Windows, podemos ir em Disk Management:

![](../media/disks8.png)

Escolher o espaço disponível no Volume e a letra para o diretório NFTS.

## Resize de Disco

Agora, para fazer o resize do disco, podemos fazer de uma forma mais fácil. Para os dois SO, temos que aumentar o disco na console(ou via gcloud):

```
gcloud compute disks resize <NOME DO DISCO> --zone <zone do disco> --size <novo tamanho, nunca para baixo>
```

Para que o SO reconheça o disco, podemos fazer um reboot da máquina. Ou podemos fazer no Linux:

1. crescer a partição
2. crescer o filesize
```
sudo su
lsblk
growpart /dev/sda <qual partição vc quer crescer>
resize2fs /dev/sda1
lsblk
```

No Windows:

Vamos no Disk Management e clicamos em Extend Disk, no disco que queremos aumentar:
![](../media/disks9.png)