 # Image

Imagens tem o propósito de criar instâncias, podemos assim configurar templates para essas instâncias.
O recomendado é que desliguemos a máquina para que uma imagem seja criada.

Podemos criar imagens de algumas fontes:
- de algum disco
- de outra imagem(no mesmo projeto)
- imagem de outro projeto
- uma image que está no Cloud Storage(linux, no formato de tar.gz) - windows não funciona.

Quando temos uma imagem customizada, podemos criar famílias de imagens, como grupos de versionamento, de uma mesma aplicação. Assim podemos fazer o roll forward ou roll back das imagens.
A família sempre aponta para a versão mais recente.

As imagens mais antigas ficam deprecadas, as imagens possuem alguns estados:
- deprecadas: ainda funcionam, mas com warning
- obsoletas: nao podemos criar uma nova instancia, somente as que já existem funcionam
- deletadas: não podemos mais usar
- ativas: é a versão corrente

As imagens podemos ser trocadas entre projetos, entretanto, é necessário uma role específica.
Caso um usuário no Projeto 1 deseja usar uma imagem do Projeto 2. O usuário precisa ter, no Projeto 2, a permissão de `Compute Engine Image User Role`.

## Criando uma Imagem Custom

Primeiramente, precisamos deixar a instância em estado de Stopped.
Dentro do menu de Compute, vamos para a seção de Images:

![](../media/image1.png)

Aqui colocamos o nome da imagem e a qual família ela pertence. Como source, podemos escolher entre:
- um disco de uma instância
- outra imagem
- um patch em algum Cloud Storage

O comando de gcloud para criar uma imagem a partir de um disco de instância seria:

```
gcloud compute --project=<nome do projeto> images create <nome da imagem> --description=<descricao> --family=<nome da familia> --source-disk=<qual o disco de origem> --source-disk-zone=<zone>
```

Para ver as imagens de uma família:

```
gcloud compute images describe-from-family <nome da familia>
```

Na console, podemos deixar uma imagem deprecada:

![](../media/images2.png)