# Snapshots

Snapshots são formas de se fazer um backup, como um periódico ponto no tempo de um disco. Podemos compartilhar esse snapshot entre projetos e zonas. Isso porque eles podem se tornar o boot disk de uma instância.

A cada snapshot de uma instância, temos o que é conhecido como *incremental backup*. Sendo somente a diferença entre snapshots sequenciais. Isso diminui o tamanho total dos arquivos. E mesmo que algum snapshot da cadeia seja apagado, os dados das mudanças são transferidos para o próximo.

![](../media/snapshots1.png)

## Best Practices

- Pare a aplicação, para que nada esteja escrevendo no disco
- Faça o `unmount` do disco, se possível.
- Para Windows use o VSS Snapshots
- Para Linux use o formato `ext4` no filesystem. Use também o comando `fstrim`.
- Não tire snapshots em horários de picos

---

Para tirar um snapshot, apenas vá no menu Compute Engine -> Snapshots:

![](../media/snapshots2.png)

Para usar a command line:

```
gcloud compute --project=<project> disks snapshot <nome da instância> --zone=<zone> --snapshot-names=<nome do snapshot>
```

O primeiro snapshot será um full do disco. Após isso, teremos o processo de incrementais:

![](../media/snapshots3.png)

Para ver todos os snapshots usando a command line:

```
gcloud compute snapshots list
```

Para ver os detalhes de algum snapshot específico:

```
gcloud compute snapshots describe <nome do snapshot>
```