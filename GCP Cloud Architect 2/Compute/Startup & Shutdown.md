# Startup and Shutdown

Podemos criar scripts para inicialização e desligamento das instâncias. Isso pode nos auxiliar em tarefas de automação em grandes números de máquinas.

Isso é muito útil quando temos que escalar nossa aplicação de uma forma rápida.

Os scripts são sempre em Kernel Mode, e são rodados em:

- Bash
- Python
- CMD

Para os scripts podemos usá-lo de duas formas:
1. com um input direto nas propriedades da instância
2. com um script no Google Cloud Storage

Para os scripts de shutdown, temos que tomar cuidado com o tempo de execução do script. Pois se o script não terminar até o tempo do shutdown, a instância desliga.

## Metadata

Podemos acessar dados sobre a instância de uma forma programática, além de colocar valores customizados na instância. Todos no tipo de Key-Value.

## Inserir um script

Podemos inserir o script nas configurações de uma instância pela console:

![](../media/startup1.png)

Ou podemos colocar uma referência a um script em um Cloud Storage:

![](../media/startup1.png)

A Key deve ser exatamente essa.