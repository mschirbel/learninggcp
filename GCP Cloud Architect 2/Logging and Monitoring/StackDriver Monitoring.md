# StackDriver Monitoring

StackDriver Monitoring permite monitorar ambientes no GCP e na AWS. Podemos monitorar serviços e aplicações de terceiros também.

Existem dois tiers:
- basic
- premium

Mas são separados no Billing, e tem diferenças nos produtos de dentro do StackDriver.

![](../media/stackdriver1.png)

Temos métricas como:
1. cpu
2. disk
3. network
4. uptime

Isso sem usar o agent. Caso escolhemos usar o agent, podemos ter mais detalhes sobre as aplicações internas.
---

## Setup

Precisamos acessar o StackDriver:

![](../media/stackdriver2.png)

Já começamos o Free Trial do Premium quando iniciamos o StackDriver.
Depois escolhemos qual projeto o StackDriver vai monitorar:

![](../media/stackdriver3.png)

Se houver algum recurso na AWS, precisamos adicionar uma role:

![](../media/stackdriver4.png)

Depois recebemos uma informação de como instalar o agent:

![](../media/stackdriver5.png)

Agora temos acesso a dashboards sobre a nossa monitoração.

## Account Settings

Podemos adicionar as contas que precisam ser monitoradas, ou ver quais as permissões temos por projeto:

![](../media/stackdriver6.png)

E podemos colocar algumas notificações:

![](../media/stackdriver7.png)

## Resources Monitoring

Podemos ver os tipos de recursos que temos nos projetos, ou até mesmo criar uma métrica *on-point* para monitorarmos os ambientes:

![](../media/stackdriver8.png)

## Install Agent

Para instalar o agent é bem simples, e tem um passo a passo na tela do StackDriver:

![](../media/stackdriver9.png)

Depois de instalado, é bom configurar Grupos, para policies de alertas próprios para o grupo:

![](../media/stackdriver10.png)

## Dashboards

Podemos criar dashboards, sejam para os projetos ou para grupos, para termos gráficos das informações:

![](../media/stackdriver11.png)

#### Uptime Checks

Podemos criar um check de uptime para saber a disponibilidade de nossos serviços. Podemos colocar uma URL, por exemplo:

![](../media/stackdriver12.png)

E agora podemos ter uma notificação caso o uptime check falhe:

![](../media/stackdriver13.png)