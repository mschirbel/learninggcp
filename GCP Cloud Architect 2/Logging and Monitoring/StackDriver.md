# StackDriver Overview

É um conjunto de ferramentas para Logging e Monitoring. Pode ser usado tanto na GCP quanto na AWS.

Era uma ferramenta de terceiro que o Google comprou. Ele pode dinamicamente descobrir os recursos GCP e pode se integrar com VM's usando um agent.
Existem cinco produtos:

1. Monitoring
Cria métricas de monitoração, cuida de health-checks e cria dashboards e alertas
2. Logging
Atividades para auditoria, e salva em log.
3. Error Reporting
Identifica e entender erros de aplicações
4. Trace
Encontra *bottlenecks* de latencia nos projetos
5. Debugger
Encontra e corrige erros de códigos

---

Tem uma função de multicloud, pois é nativo do GCP e na AWS. Ele pode identificar os problemas até antes de acontecer, sejam eles erros de códigos ou até mesmo latência. E com isso, podemos centralizar as logs em um único cloud.

Existem também diversas integrações com outras ferramentas de terceiros, como Splunk, Atlassian Tools e PagerDuty.

Para melhorar o ambiente, podemos usar um único ambiente para termos todos os dados do StackDriver. Com isso, podemos determinar nossas necessidades - sejam de monitoração e sejam de acesso.