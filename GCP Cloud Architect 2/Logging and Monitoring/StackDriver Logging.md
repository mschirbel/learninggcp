# StackDriver Logging

Podemos guardar, analisar, procurar entre e alertar tudo relacionado a logs, seja da GCP ou da AWS.
Funciona como um repositório central nossas aplicações e eventos.

Podemos também, claro, exportar para outros serviços.

## Conceitos

O Logging é feito por projeto, só podemos ver as logs de um projeto por vez.
*Log entry* é uma linha de log, como por exemplo a entrada de uma aplicação.

*Logs* é um conjunto de *log entries*, que ficam disponíveis por um período de retenção.

## Tipos de Logs

1. Audit Log
São dois tipos que existem:
- Admin Activity - são feitas quando existe alguma modificação de serviços. Vem habilitada por default e não tem custo. Tem retenção de 400d.
- Data Access - vem desabilitada e precisa de role para ter o acesso. Registram mudanças nos dados de usuários. Tem retenção de 30d.

2. Non-Audit Log
Todas as outras logs, tem retenção de 30d.

## Export

Podemos exportar para:
- Cloud Storage
- BigQuery
- Pub/Sub

Ou seja, podemos fazer o export em blocos, ou em stream para análise.
Nisso, podemos incluir filtros para nem todas as logs sejam guardadas/analisadas.

Quando vemos as logs na tela do StackDriver, podemos selecionar entre diversos filtros:

![](../media/stackdriver14.png)

E depois de criado o filtro, simplesmente clicamos em *Create Export* e selecionamos o destino.

![](../media/stackdriver16.png)
*Sink* é o nome dado para o destino das logs, como um conjunto das logs indo para um destino comum.

Caso vá para o BigQuery, teremos um tabela assim:

![](../media/stackdriver17.png)

## Space

Devemos ter consideração do espaço usado para nossas logs:

![](../media/stackdriver15.png)

## Enable Data Access Logs

Primeiramente precisamos fazer uma policy para nosso projeto:

```
gcloud projects get-iam-policy <NOME DO PROJETO> > policy.yaml
```

E devemos editar a log, acrescentando a seguinte config no topo do arquivo:

```yaml
auditConfigs:
- auditLogConfigs:
    - logType: ADMIN_READ
    - logType: DATA_WRITE
    - logType: DATA_READ
    service: allServices
```

Depois aplicamos as mudanças.

```
gcloud projects set-iam-policy <NOME DO PROJETO> policy.yaml
```