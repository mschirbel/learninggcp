# StackDriver Trace

## Error Reporting

Podemos tomar ações em tempo real com erros em alertas nas aplicações.
É feito automaticamente no App Engine e tambem podemos usar para containers e EC2(AWS), mas precisa do agent do Logging instalado.

Podemos ver quais os tipos de erros estão acontecendo em nossa aplicação:
![](../media/stackdriver19.png)

E até mesmo qual linha de código:
![](../media/stackdriver20.png)

## Trace

Podemos encontrar bottlenecks e latências nas aplicações, coletando informações do:
- app engine
- load balancers
- apps com sdk do stackdriver trace

Pode ser instalado em recursos fora da GCP.

Com o Trace, podemos saber motivos da lentidão em requests e o que podemos fazer para reduzir.

![](../media/stackdriver18.png)

## Debugger

Funciona com algumas linguagens, como Java, Python, Go e NodeJs.
E com essa ferramenta podemos saber problemas em tempo real das aplicações e ainda podemos instalar fora da GCP.