# Versioning & Lifecycle

## Versioning

Por default, se apagarmos um objeto, ele é apagado pra sempre. Mas quando temos o versionamento habilitado, os arquivos não são mais apagados, mas sim, colocados em um estado de apagado, podendo ser recurperado posteriormente.  
Cada objeto terá um ID para localização, isso inclui suas versões passadas.  

E temos a possibilidade de deletar versões antigas para sempre. E não existe limite para as versões dos arquivos.
E para cuidar desse crescimento, temos o lifecycle.

As permissões de ACL são atreladas ao objeto, mesmo se mudarmos posteriormente, ela fica com as antigas.

Para usar o Versioning com o `gsutil` só temos 3 opções:
1. habilitar o versioning
2. desabilitar o versioning
3. ve o estado corrente de versionamento

```shell
# check versioning state
gsutil versioning get gs://<bucket>

# turn versioning on
gsutil versioning set on gs://<bucket>

# turn versioning off
gsutil versioning set off gs://<bucket>

# check all files on bucket
gsutil ls -a gs://<bucket>

# restore an archived file
gsutil cp gs://<bucket>/<file with GENERATION ID> gs://<bucket>/<file without GENERATION ID>

# remove permanently a file
gsutil rm gs://<bucket>/<file with GENERATION ID>
```

### Properties

1. Generation - é trocado quando o objeto é sobrescrito.
2. Metageneration - metadados de um generation

## Lifecycle

Temos a possibilidade de, dado uma *condição*, tomarmos uma *ação*, respeitando uma *regra*, sobre os objetos de um bucket.

Por exemplo:
- não manter mais do que 3 versões de um arquivo
- deletar objetos anteriores a 2015
- mover objetos para outro bucket.

### Rules

Um conjunto de condições para tomar uma ação. Se múltiplas condições existem para uma regra, todas devem ser executadas:

![](../media/storage7.png)

### Conditions

Existem 5 tipos de conditions:
1. Age - tempo de vida de um objeto
2. CreatedBefore - quando o objeto for criado
3. IsLive - se é a versão arquivada de um objeto
4. MatchesStorageClass - se um objeto está em uma classe específica
5. NumberOfNewerVersions - se existem N versões de um objeto.

### Actions

1. Delete - deletar um objeto. Se deletarmos uma versão live, apenas arquivamos o objeto. Mas se deletamos um objeto arquivado, aí ele é deletado pra valer.
2. SetStorageClass - podemos mover os objetos entre Storage Classes

*Lembre que não podemos ir de regional -> multi-regional e vice-versa*.

Regional Standard -> Regional Nearline/Coldline
Multi-Regional Standard -> Multi-Regional Nearline/Coldline.

## Create Lifecycle

Para criar um lifecycle, clicamos na área do bucket:

![](../media/storage8.png)

Adicionamos uma rule, colocando uma condition e uma action:

![](../media/storage9.png)

![](../media/storage10.png)

Podemos editar uma rule, ou até mesmo criar outras diversas rules para o nosso bucket.

Ou podemos criar usando JSON:

```json
{
    "rule": {
        {
            "action": {
                "type": "Delete"
            },
            "condition": {
                "numNewerVersioons": 3
            }
        }
    }
}
```

Para usar como json:

```shell
# get versioning json policy
gsutil versioning get gs://<bucket> > policy.json

# set versioning json policy
gsutil versioning set policy.json gs://<bucket>
```