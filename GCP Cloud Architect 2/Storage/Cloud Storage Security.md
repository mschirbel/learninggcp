# Cloud Storage Security

Existem dois métodos para a segurança:
- IAM
- ACL

## IAM

Se aplicam a toda a GCP, recebendo hierarquia do projeto e do recurso.
Entretanto, não conseguimos dar acesso no nível do objeto, apenas a nível do bucket.

#### IAM Roles

Temos as roles primitivas de projeto(Owner, Editor, Viewer), mas para o cloud storage temos as *Standard Storage Roles*(independente do ACL):
- storage admin
- storage object admin
- storage object viewer
- storage object creator

E temos também as *Legacy Roles*, que funcionam em conjunto com as ACL
- storage legacy bucket owner
- storage legacy bucket reader
- storage legacy bucket writer
- storage legacy object owner
- storage legacy object reader
</br>
*Para uma pessoa criar arquivos em um bucket, ela precisa ter storage object creator e storage object viewer*</br>

Para conceder acesso a uma pessoa, podemos fazer via console:

![](../media/storage2.png)

Para conceder acesso a uma pessoa, usando a cli:

```
gsutil iam ch user:<EMAIL DO USER>:<ROLES SEPARADAS POR ','> gs://<NOME DO BUCKET>
```
Os comandos estão na documentação oficial.

## ACL

Access Control List. Só existem no Cloud Storage e em nenhum outro recurso da GCP. Elas definem acesso ao nível do objetos dentro de buckets.  
Mas quando possível, devemos usar permissões do IAM. Elas podem ser:
1. Owner
2. Writer
3. Reader

Para vermos quais são as ACL já atribuidas a um objeto:

![](../media/storage3.png)

E podemos trocar:

![](../media/storage4.png)

Para atribuir via cli:

```
# no bucket:
gsutil acl ch -u <EMAIL>:<acl permission> gs://<bucket name>

#em um objeto especifico:
gsutil acl ch -u <EMAIL>:<acl permission> gs://<bucket name>/<subfolder>/<object>
```

## Overlap

Existe uma interseção entre IAM e ACL, e existe.  
Mas como melhores práticas, devemos sempre usar IAM, pois isso é controle empresarial das permissões, que nos dá controle de auditoria.  
Mas sabemos que o ACL permite um acesso mais granual que, sim, pode ser útil em algumas situações.

## Signed URLs

Existe um terceiro tipo de acesso, que é o Signed URL, no qual concedemos acesso a um bucket, ou objeto, para uma pessoa que não necessariamente tem uma conta no Google.

Esse acesso é temporário e **qualquer pessoa com aquele link tem acesso no conteúdo do bucket**.

Precisamos ir no API & Services e criar uma credencial:
![](../media/storage5.png)

Crie uma:
![](../media/storage6.png)

Fazemos o download da key. E fazemos o upload dessa key para nosso cloud shell.

E para fazer a Signed URL:

```
# para 10min
gsutil signurl -d 10m <PATH DA KEY> gs://<nome do bucket>

# para um arquivo especifico
gsutil signurl -d 10m <PATH DA KEY> gs://<nome do bucket>/<subfolder>/<object>
```