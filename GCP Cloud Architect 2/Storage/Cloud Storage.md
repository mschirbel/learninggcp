# Cloud Storage

Ele serve para dados que não tem um formato estrutural definido, assim como aqueles que estão em um DB.
O tamanho é virtualmente ilimitado, podendo ir até os exabytes.

Uma unidade do Cloud Storage é um *bucket*, que controlamos o acesso via IAM. Dentro do bucket, colocamos *objects*, sejam eles arquivos ou pastas. Os objetos tem a mesma permissão e classe do bucket.

## Classes

Regional - somente uma region, uso para acesso fácil em uma região<br/>
Multi-Regional - em várias regions, e usamos para acesso rápido quando nossa aplicação está em diversas regiões<br/>
Nearline - regional ou multi-regional, usamos para arquivos com baixo acesso e tem um custo de retirada.<br/>
Coldline - regional ou multi-regional, usamos para dados com baixíssimo acesso e tem custo de retirada.

Lembrando que:

- não podemos trocar de multi-regional para regional
![](../media/storage1.png)
- podemos transferir objetos para outros buckets. Se forem de mesma classe, podemos usar a web console, se forem de classes diferentes temos que usar o `gsutil`.
- nearline e coldline podem ser regional ou multi-regional.

## gsutil

É uma aplicação em Python que permite o acesso para o Cloud Storage. E somente para o Cloud Storage.

O formato é o seguinte:
`gsutil <comando> <opção> <alvo>`

E o formato de um bucket é o seguinte:
`gs://<nome do bucket>/<subpasta>/<arquivo>`

Podemos ver os comandos ![aqui](https://cloud.google.com/storage/docs/gsutil).