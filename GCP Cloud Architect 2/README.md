# GCP Cloud Architect 2

- Account
    1. Organization
    2. Quotas
    3. Labels
- IAM
    1. Overview
    2. Service Account
    3. Best Practices
    4. Billing
- Logging and Monitoring
    1. StackDriver Overview
    2. StackDriver Monitoring
    3. StackDriver Logging
    4. StackDriver Tracing
- Storage
    1. Cloud Storage
    2. Cloud Storage Security
    3. Object Versioning & Lifecycle
- Network
    1. Network Overview
    2. VPN Overview
    3. Cloud VPN
    4. Cloud Router
    5. VPC
    6. Firewall
- Compute
    1. Disks
    2. Snapshots
    3. Startup & Shutdown
    4. Scaling
        - Load Balancer
        - Instance Group
        - Autoscaling
        - Cloud Deployment Manager
- Container
    1. GKE
- Development
    1. Management Tools
- Migration Cases

- Security