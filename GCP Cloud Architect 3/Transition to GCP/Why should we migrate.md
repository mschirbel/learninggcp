# Why should we migrate?

Isso é uma pergunta muito importante para um arquiteto de soluções. Porque as decisões dos CEO, COO, CTO impactam muito a vida dos negócios.

Temos alguns motivos que podemos salientar:

- Custos

Claro que existem muitos problemas com isso, porque muitas pessoas acabam não sabendo como fazer isso. Muito porque trocamos o CapEx por OpEx(custo de produção por custo de operação).

Existem também recursos em nuvem que são pagos somente pelo uso.

- Future-proo Infra

O hardware não fica gasto, isso é ótimo para gastos e para o pessoal que cuida dele. Com isso, teremos menos trabalho

- Escalabilidade

Temos a chance de ter uma computação distribuida e podemos criar ou destruir recursos conforme temos necessidade.

- Agilidade

Com a nuvem podemos criar recursos e analisar mais dados de forma mais rapido. Isso agiliza as decisões do negócio.

- Serviços Gerenciados

A GCP automatiza o gerenciamento de vários serviços, como DB, Storage, Network. Ou seja, coisas a menos que tomam o tempo.

- Alcance Global

Podemos ter recursos em todo o globo. Isso pode ser útil para serviços que são multinacionais.

- Segurança

Como os serviços são gerenciados por um Cloud Provider, eles são ótimos em encontrar e corrigir vulnerabilidades. Fora o IAM, que permite controlar granularmente as permissões dos usuários.