# Storage Transfer

É um serviço que permite dados(somente online) para a GCP. Ou seja, podemos ter fontes como:
- S3 Bucket
- HTTP location
- Cloud Storage

E para isso, fazemos um *sink* em um Cloud Storage Bucket.

## Possibilidades

Podemos usar isso para fazer backups, seja na AWS ou em outros locais online.
Essa janela de transferência pode ser feita em uma única vez ou em vezes recorrentes, isso é chamado de *transfer job*.

Isso tudo pode ser protegido via autenticação e autorização.

## Diferença entre Storage Transfer e Gsutil

O `gsutil` nos permite usar copias de *on-prem* locations, ao contrário que o Storage Transfer que só nos permite usar de dados **online**. Como podemos ver abaixo:

![](../media/transfer-data.png)
