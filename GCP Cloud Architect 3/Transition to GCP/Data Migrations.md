# Data Migrations

Migrar dados depende basicamente de dois fatores:
- quantidade de dados
- largura de banda

Podemos ver isso na imagem abaixo:

![](../media/data1.png)

*Close e Far seriam termos que determinam a velocidade de upload final dos seus dados*.

Isso determina o tipo de estratégia de upload que vamos fazer, sendo com ferramentas de terceiros, Storage Transfer ou até mesmo o `gsutil`.

Para deixar o processo mais rápido podemos:
- diminuir o tamanho dos dados - isso é importante principalmente para dados duplicados.
- aumentar o tamanho da banda - isso inclui em maior custo.

Se tivermos, por exemplo, muitos arquivos pequenos é melhor comprimir tudo em um único arquivo e transferi-lo.

Para arquivos muito grandes, podemos quebra-los em uploads paralelos.

### Gsutil Limitations

- não tem network throttling
- não é bom para transferências automatizadas - melhor usar algun cron job

### Upgrade Gsutil

- Podemos usar o `-m` no `gsutil` para usar multi-thread.
- Podemos usar `parallel composite uploads` com o `-o GSUtil:parallel_composite_upload_threshold=150M`
- Retry & Resume, podemos pausar e continuar ou até mesmo tratar erros

## Transferência Física

Tipo o Snowmobile da AWS, temos aqui o Google Transfer Appliance. Bom para mais de 20TB de dados.

## Soluções de Storage

Também devemos saber como os diferentes tipos de dados devem ser estruturados na nuvem.

| Type of Data        | Service               |
|---------------------|-----------------------|
| Unstructured Data   | Cloud Storage         |
| Relational Data     | Cloud SQL ou Spanner  |
| Non-relational Data | BigTable ou DataStore |
| Big Data Analysis   | Big Query             |
| In memory DB        | Cloud Memorystore     |
| Other               | Persistent Disk       |

E podemos seguir um fluxograma para entender as necessidades de cada solução:

![](../media/data2.png)