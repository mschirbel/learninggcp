# Migrations

Antes de começar uma migração, precisamos pensar na arquitetura de nossa aplicação para aproveitarmos o máximo que a Cloud pode nos proporcionar. Para isso, pensamos em alguns princípios fundamentais:

- High Availability
- Scalability
- Security
- Disaster Recovery
- Costs

## High Availability

"Será que os usuários podem acessar nossa aplicação com a menor latência possivel e sabendo que em caso de falha, nossa aplicação não sofrerá pausas/lentidão?"

Essa é a pergunta que devemos fazer para esse principio. Para isso devemos considerar o perfil dos nossos usuários, geolocalização e publicidade.

Podemos usar serviços como Global Load Balancing, Multi Regions e etc.

## Scalability

"Será que os recursos precisam variar conforme a demanda?"

É aqui que podemos usar serviços como GCE, GKE, GAE para considerar os altos níveis de uso de nossa aplicação.

## Security

"O acesso a nossa aplicação, seja externo ou interno, é somente o necessário?"

O princípio da segurança é um dos mais primordiais para evitar problemas no futuro, como ataques hacker e malwares indesejados.

Sempre tendo em mente o principio do `least privilege`. E usando redes de Firewall e IAM Roles

## Disaster Recovery

"O que fazemos se tudo der errado?"

Ter um plano de backup, ou até mais que um, é necessário para o mundo moderno de aplicações em cloud. Sempre interessante promover a cultura de backups, snapshots, servidores de failover e trazer o rollback de aplicações em caso de falha no deploy.

## Costs

"Será que estou gastando o mínimo que preciso?"

Podemos tratar a escalabilidade como algo que nos ajuda em relação aos custos, desde que usemos as capacidades de computação de forma inteligente, conforme a demanda de nossa aplicação.
Outro ponto interessante, é o uso de Preemtible VM's e também de Custom Machine Types.