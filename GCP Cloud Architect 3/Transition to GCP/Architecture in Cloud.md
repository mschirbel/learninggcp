# Architecture in Cloud

Tudo isso vem da pergunta: "Como colocar todas as coisas na nuvem?"

GCP recomenda 5 fases:
- Assess
- Pilot
- Move Data
- Move Applications
- Cloudify and Optimize

## Assess

Temos que saber as dificuldades de cada recurso que será migrado para a nuvem. Devem ser:
1. Fácil de mover para nuvem
2. Difícil de mover para a nuvem
3. Não podemos mover para a nuvem

Isso deve entrar em consideração com diversos fatores, tais como:
- criticidade da aplicação
- compliance
- licenças
- ROI

Temos que migrar primeiramente aquilo que vai ter o maior impacto

## Pilot

É o MVP da nuvem. Para isso, não queremos nada que seja crítico ou muito difícil. O mais importante e tomar passos pequenos para essa fase.

Sempre em mente algum rollback para saber voltar para um estado funcional.

É nesse passo que temos o mapeamento de:
- VPC
- Ambientes(dev, hmg e prd)
- Projetos
- Funções
- etc

## Move Data

É sempre bom mover os dados antes das aplicações por questões de dependências. E devemos saber quais ferramentas podemos usarr para mover e guardar os dados. Por exemplo:

![](../media/arch1.png)

## Move Applications

Podemos mover as aplicações, ou usar ferramentas para fazer isso. E sempre tentando fazer um cenário de `lift-and-shift`. Onde apenas copiaremos o ambiente para a nuvem.

Lembre-se que a GCP pode servir como ambiente principal, ambiente misto ou até mesmo ambiente de DR.

![](../media/arch2.png)

## Cloudify and Optimize

Aqui, devemos usar ferramentas cloud-natives para melhor performance e gerenciamento de aplicações e dados na nuvem. Como por exemplo, auto scaling groups, global LB e etc.

Também devemos considerar ferramentas de monitoração próprias para cloud.
