# Cost Optimization

Em princípio, isso deve ser algo que varia de negócio para negócio e todas os descontos podem ser testados para ver a eficiência em comparação com a performance.

## Running VM

Quando estamos usando muito o Compute Engines ou o Cloud SQL, a GCP nos oferece 30% de desconto pelo tempo de uso das instancias.

Não precisa de pagamento *up-front*.
O jeito de entender como funciona é o seguinte:

![](../media/cost1.png)

Isso também pode ser aplicado para tipos customizados de instancias. A GCP faz recomendações de troca de tipo de instancias, para melhorar a otimização do uso de CPU e RAM.

Podemos também usar Preemptible VM, que seria basicamente as Spot Instances na AWS. Mas a diferença é que não tem um *bid*, ou seja, o preço é fixo e temos um total de 80% de desconto.

## Storage

Para Storage, podemos usar o Coldline Storage, que é como o Glacier na AWS. Podemos colocar aqui dados que não temos a necessidade constante de termos acesso. Mas a diferença do Glacier é que podemos acessar com mais rapidez os dados, como um Cloud Storage Bucket normal.
