# Migrating Applications

É necessário entender que mover aplicações do *on-prem* para a cloud, envolve muitos outros serviços, como Redes, Storages, Management e etc.

Podemos ver as diferenças e comparações entre eles abaixo:

![](../media/migrate1.png)

Sempre tenha em mente que, antes de pensar em quais serviços devemos usar, pense em como estará distribuído os projetos da sua empresa. Isso impacta a camada de rede entre as aplicações e principalmente questões de autenticação e autorização.

## Images

Podemos fazer um *lift and shift* das imagens que estão em servidores para as VMs da GCP. Mas o ideal é que usemos imagens públicas, que são repetiddamente atualizadas por times especializados.

Mas ainda assim, podemos migrar um boot disk, para isso, temos duas possibilidades:

- criar um snapshot do disco, criar um ` .tar.gz` e mover para a GCP e depois importar como uma imagem customizada. Isso só funciona para Linux.
- podemos usar o CloudEndure VM Migration. Isso funciona para Linux e Windows(>2008).

*as licenças são movidas para a GCP, e vc paga no Billing, normalmente*.
