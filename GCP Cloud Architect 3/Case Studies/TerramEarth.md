# TerramEarth

É uma aplicação de produtora de equipamentos pesados para agriculturas e mineradoras.

A parte de tecnologia, está nos veículos. Eles coletam dados sobre o uso e enviam para a stack na GCP que depois será usada para predições e análises.

Usando um total de 9TB de dados por dia, e 24x7 em straming. Portanto, o desafio é ter agilidade nas decisões de negócio.

Uma arquitetura possível seria:

![](../media/case2.png)