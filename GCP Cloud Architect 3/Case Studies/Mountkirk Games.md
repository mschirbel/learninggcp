# Mountkirk Games

Para cada um dos casos de estudos da prova, teremos:
- contexto
- requisitos de negócio
- requisitos tecnicos

Tudo isso deve ser levado em conta quando estudamos um caso.

## Problem

O deploy da primeira release foi feito em uma infraestrutura pequena e não escalável. Agora, querem migrar para a GCP.

Querem usar GCE, com NoSQL e Analytics.
Isso inclui os serviços :
- Datastores
- BigQuery
- Cloud Storage
- Stackdriver
- Global HTTP LB
- Multiregion

É interessante que para jogos, podemos usar:
- bigquery, para escalabilidade entre múltiplas regioes
- cloud datasore, que é um NoSQL para games.

## Architecture

![](../media/case1.png)