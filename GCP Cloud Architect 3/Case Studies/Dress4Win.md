# Dress 4 Win

Para esse caso, temos alguns objetivos principais:
- escala
- contenção de custos
- eliminar recursos inutilizados

Para isso, podemos dividir os ambientes(dev e prod), em diferentes projetos.
Caso seja necessário usar On-Prem, podemos criar estratégias de DR.

Com isso, podemos traçar estruturas de deployment para os recursos em nuvem. Quanto mais automações, melhor.

## Stackdriver

Podemos usar o Stackdriver para monitor as aplicações e ver as logs, também.