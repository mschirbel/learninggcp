# GCP Cloud Architect 3

## Lessons

1. Case Studies
    - Mountkirk Games
2. Transition to GCP
    - Why should we migrate?
    - Cost Optimization
    - Migration
    - Architecture in Cloud
    - Storage Transfer
    - Migrating Applications
    - Data Migrations